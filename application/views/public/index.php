<div ng-controller="MainController">
  <div id="wrapper" class="toggled">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <div class="panel-group" id="accordion">

              <div class="panel panel-default">
                <a href="#collapse_POI" data-toggle="collapse" data-parent="#accordion" id="poi">
                  <h4 class="legend-header">
                    <span class="poi"><i class="fa fa-minus"></i></span> POI Layers
                  </h4>
                </a>
                <div id="collapse_POI" class="panel-body panel-collapse collapse in">

                      <div ng-repeat="layer in layers" class="layer visible" ng-click="toggleVisiblityOfLayer(layer.id)">
                        <div class="visibleIndicator">
                          <i ng-show="layer.visible" class="fa fa-check-square"></i>
                        </div>
                        <img ng-src="{{layer.icon}}" class="legend">
                        <a class="name">{{layer.name}}</a>
                      </div>
                      <div class="layer visible" ng-click="hideAll()">
                        <div class="visibleIndicator"></div>
                        <a class="name">Hide All</a>
                      </div>
             
                </div>
              </div>

              <div class="panel panel-default">
                <a href="#collapse_thematic" data-toggle="collapse" data-parent="#accordion" id="thematic">
                  <h4 class="legend-header">
                    <span class="thematic"><i class="fa fa-plus"></i></span> Thematic Layers
                  </h4>
                </a>
                <div id="collapse_thematic" class="panel-body panel-collapse collapse">

                  <!-- inner collapsibles -->
                  <div class="panel-group" id="accordion2">
                      
                      <div ng-repeat="layer in thematicLayers" class="panel layer visible">
                          <div class="visibleIndicator">
                            <i ng-show="layer.visible" class="fa fa-check-square thematic-legend"></i>
                          </div>
                          <a href="#collapse-{{layer.id}}" data-toggle="collapse" data-parent="#accordion2" ng-click="toggleVisiblityOfThematicLayer(layer.id)" class="name-thematic">{{layer.name}}</a>
                          <div id="collapse-{{layer.id}}" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div ng-repeat="legend in layer.legend">
                                <img ng-src="{{legend.icon}}" class="thematic-legend">
                                <p class="label">{{legend.label}}</p>
                              </div>
                            </div>
                          </div>
                      </div>

                      <!-- hide all -->
                      <div class="panel">
                        <div class="layer visible">
                          <a href="#collapse-hideAll" data-toggle="collapse" 
                          data-parent="#accordion2" ng-click="hideAllThematicClicked()" class="thematic-hide">Hide All</a>
                          <div id="collapse-hideAll" class="panel-collapse collapse">
                            <div class="panel-body">
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                  <!-- /inner collapsibles -->

                </div>
              </div>

            </div>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->


      <div class="row-fluid padding10">
          <div class="row-fluid mainMapContainer">
           	<div class="col-xs-12 fullheight mapcontainer" style="padding-left: 0px; padding-right: 0px;">
            	<div id="main_map">
                <div id="HomeButton"></div>
                <button class="advancedSearch" data-toggle="modal" data-target="#search"><i class="fa fa-search"></i></button>
                <select class="countyZoom" ng-model="kyCounty" ng-options="kyCounty.attributes.COUNTY for kyCounty in allKentuckyCounties" ng-click="zoomToCounty()">
                  <option value="">Select A County</option>
                </select>
              </div>
           	</div>
         </div>  <!-- mainMapContainer -->
      </div> <!-- /container -->

  </div> <!-- /wrapper -->


  <!-- Advanced Search Modal -->
  <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel">Advanced Search</h4>
        </div>
        <div class="modal-body">
          <select ng-model="layer" ng-options="layer.name for layer in layers" ng-change="setSearch()">
            <option value="" selected>-Select a layer-</option>
          </select>

          <div ng-show="layer">

            <!-- MARKET PRODUCE AND TYPES -->
            <div ng-show="farmersMarketItemsExist" class="ng-hide">
              <br>
              <select class="dynamic-select" ng-model="item" ng-options="item.name for item in farmersMarketItems" ng-change="filterCountiesByItem(); setFarmersMarketItemTypes()">
                <option value="" selected>-Select An Item-</option>
              </select>
              <em><small>(Optional)</small></em>
            </div>
            <div ng-show="retrievingItems">
              <i class="fa fa-spinner fa-spin"></i> Getting items...
            </div>

            <div ng-show="farmersMarketItemTypesExist" class="ng-hide">
              <br>
              <select class="dynamic-select" ng-model="itemType" ng-options="itemType.name for itemType in itemTypes" ng-change="filterCountiesByItem();">
                <option value="" selected>-Select A Type-</option>
              </select>
              <em><small>(Optional)</small></em>
            </div>
            <div ng-show="retrievingItemTypes">
              <i class="fa fa-spinner fa-spin"></i> Getting types...
            </div>

            <!-- INDUSTRIES AND SUBCATEGORIES -->
            <div ng-hide="layer.id == 7">
              <div ng-show="industriesExist" class="ng-hide">
                <br>
                <select class="dynamic-select" ng-model="industry" ng-options="industry for industry in industries" ng-change="filterCounties(); filterSubcategories()">
                  <option value="" selected>-Select an industry-</option>
                </select>
                <em><small>(Optional)</small></em>
              </div>
              <div ng-show="retrievingIndustries">
                <i class="fa fa-spinner fa-spin"></i> Getting industries...
              </div>

              <div ng-show="subcategoriesExist" class="ng-hide">
                <br>
                <select class="dynamic-select" ng-model="subcategory" ng-options="subcategory for subcategory in subcategories" ng-change="filterCounties(); filterIndustries()">
                  <option value="" selected>-Select a subcategory-</option>
                </select>
                <em><small>(Optional)</small></em>
              </div>
              <div ng-show="retrievingSubcategories">
                <i class="fa fa-spinner fa-spin"></i> Getting subcategories...
              </div>
            </div>

            <!-- EQUINE OPTIONS -->
            <div ng-show="layer.id == 7">

              <p>Display locations that offer...</p>

              <div>
                <a style="cursor: pointer;" ng-click="toggleSelectAllEquineOptions()">
                  Check/Uncheck All
                </a>
                <small>(Selecting zero options will display all locations)</small>
              </div>
              <div ng-repeat="equineOption in equineOptions">
                <input type="checkbox" name="selectedEquineOptions[]" class="equine-options" value="{{ equineOption.value }}" id="{{ equineOption.value }}">
                <label class="display-label" style="margin-bottom: 0px;" for="{{ equineOption.value }}">
                  {{ equineOption.label }}
                </label>
              </div>

            </div>

            <h5><strong><u>Display Options</u></strong></h5>
            <div>
              <input type="radio" name="show_all" id="show_all" ng-model="searchOption" value="showAll"> 
              <label for="show_all" class="display-label">Show All </label></br>

              <div ng-hide="layer.id == 7">

                <input type="radio" name="click_radius" id="click_radius" ng-model="searchOption" value="clickRadius">
                <label for="click_radius" class="display-label">
                  Within
                  <select ng-model="mile" ng-options="mile for mile in miles" ng-click="selectClick()">
                    <option value="" selected>5</option>
                  </select>
                  miles of clicked point on map
                </label>

                <!-- only show if geolocation has been enabled -->
                <div ng-show="currentLatitude">
                  <input type="radio" name="currentLocation" id="currentLocation" ng-model="searchOption" value="currentLocation">
                  <label for="currentLocation" class="display-label">
                    Within
                    <select ng-model="currentLocationMile" ng-options="currentLocationMile for currentLocationMile in currentLocationMiles" ng-click="selectCurrent()">
                      <option value="" selected>5</option>
                    </select>
                    miles of current location
                  </label>
                </div>

                <div ng-show="countiesExist" class="ng-hide">
                  <input type="radio" name="county" id="county" ng-model="searchOption" value="county"> 
                  <label for="county" class="display-label">
                    County
                    <select ng-model="county" ng-options="county for county in counties" ng-click="selectCounty()">
                      <option value="">Select County</option>
                    </select>
                  </label>
                </div>

              </div>

            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="searchLayer()">Go</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Inventory modal -->

  <div class="modal fade" id="viewInventory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel">{{ selectedMarket.name }}</h4>
        </div>
        <div class="modal-body" ng-model="selectedMarket">

          <div style="font-size:small">
            Last Updated: {{ lastUpdatedOn }}
          </div>

          <div ng-hide="currentInventory">
            Sorry, {{ selectedMarket.name }} has not listed their inventory yet.
          </div>

          <div ng-show="currentInventory">
            Current Inventory
            <ul>
              <li ng-repeat="produce in currentInventory">
                {{produce.name}} <span ng-show="produce.type"> - {{ produce.type }}</span>
                <div class="ng-hide" ng-show="produce.details">
                  <ul>
                    <i class="fa fa-info-circle"></i> {{ produce.details }}
                  </ul>
                </div>
              </li>
            </ul>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


</div>

<script type="text/javascript">

  function getFarmersMarkets(){
    var $element = $('div[ng-controller="MainController"]');
    var scope = angular.element($element).scope();

    return scope.farmersMarkets;
  }

  $('body').on('DOMNodeInserted', 'div.farmersMarketContent', function(){

      var titleDiv = $("div.farmersMarketTitle");
      var kadis_id = titleDiv.attr('id');
      // console.log(kadis_id);

      var farmersMarkets = getFarmersMarkets();
      var selectedMarket = '';

      for(i in farmersMarkets){
        if(farmersMarkets[i].kadis_id == kadis_id){
          selectedMarket = farmersMarkets[i];
          break;
        }
      }

      //build and append the dynamic content window
      var marketInformation = "<div class='name'><b><span class='name'>"+selectedMarket.name+"</span></b></div>";

      if(selectedMarket.address != null && selectedMarket.city != null 
        && selectedMarket.zipcode != null && selectedMarket.state != null){
        marketInformation += "<div class='location'><i class='fa fa-map-marker'></i>: <span class='location'>"+selectedMarket.address+"<br/>"+selectedMarket.city+", "+selectedMarket.state +" "+selectedMarket.zipcode+"</span></div>";
      }
      if(selectedMarket.contact_name != null){
        marketInformation += "<div class='contact-name'><i class='fa fa-user'></i>: <span class='contact-name'>"+selectedMarket.contact_name+"</span></div>";
      }
      if(selectedMarket.contact_phone != null){
        marketInformation += "<div class='contact-phone'><i class='fa fa-phone'></i>: <span class='contact-phone'>"+selectedMarket.contact_phone+"</span></div>";
      }
      if(selectedMarket.website != null){
        marketInformation += "<div class='website website-wrap'><i class='fa fa-external-link-square'></i>: <a href='"+selectedMarket.website+"' target='_blank'><span class='website'>"+selectedMarket.website+"</span></a></div>";
      }
      if(selectedMarket.hours != null){
        marketInformation += "<div class='hours'><i class='fa fa-clock-o'></i>: <span class='hours'>"+selectedMarket.hours+"</span></div>";
      }

      $("div.contentPane").prepend(marketInformation);

  });

  //check all possible fields when the last div is inserted
  $('body').on('DOMNodeInserted', 'div.website', function(){

      //get rid of any whitespaces
      var website = $("span.website");
      website.html($.trim(website.html()));

      var name = $("span.name");
      name.html($.trim(name.html()));
      
      var location = $("span.location");
      location.html($.trim(location.html()));

      var contact = $("span.contact-name");
      contact.html($.trim(contact.html()));
     
      var phone = $("span.contact-phone");
      phone.html($.trim(phone.html()));

      //hide if empty
      if(website.is(":empty")){
        $("div.website").css('display', 'none');
      }
      if(location.is(":empty")){
        $("div.location").css('display', 'none');
      }
      if(contact.is(":empty")){
        $("div.contact-name").css('display', 'none');
      }
      if(phone.is(":empty")){
        $("div.contact-phone").css('display', 'none');
      }
      if(name.is(":empty")){
        $("div.name").css('display', 'none');
      }

  });

  
 //only farmers markets have hours of operation right now, but that's 
 //this field's last div, so we'll do a check for them as well
 $('body').on('DOMNodeInserted', 'div.hours', function(){

    var hours = $("span.hours");
    hours.html($.trim(hours.html()));

    if(hours.is(":empty")){
      $("div.hours").css('display', 'none');
    }

  });

  $('div.panel > a').click(function(){
      var type = $(this).attr('id');

      if($('span.'+type).html() == '<i class="fa fa-minus"></i>'){
        $('span.'+type).html('<i class="fa fa-plus"></i>');
      }
      else{
        $('span.'+type).html('<i class="fa fa-minus"></i>');

        //change the other layer icon to plus if opened
        if(type == "poi"){
          if($('span.thematic').html() == '<i class="fa fa-minus"></i>'){
            $('span.thematic').html('<i class="fa fa-plus"></i>');
          }
        } else {
          if($('span.poi').html() == '<i class="fa fa-minus"></i>'){
            $('span.poi').html('<i class="fa fa-plus"></i>');
          }
        }

      }
  });

</script>

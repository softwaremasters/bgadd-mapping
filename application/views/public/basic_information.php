<div class="container padding15">

	<div class="col-xs-12 col-sm-9 well" ng-controller="BasicInfoController">
		<form ng-submit="completeSignUpProcess()">
			<h2 class="signup">Basic Information</h2>

			<div class="col-xs-12">
				<small>
					Welcome! You're almost ready to get into your account. We just need some basic information about <?php echo $entity->name; ?>.
				</small>
			</div>
			<br>
			<br>

			<p class="alert alert-danger ng-hide col-xs-12 col-sm-10" ng-show="basicErrors">{{ basicInfoErrors }}</p>

			<div class="form-group col-xs-12 col-sm-10">
				<input type="email" ng-model="email" name="email" class="form-control" placeholder="Email" required>
			</div>
			<div class="form-group col-xs-12 col-sm-10">
				<input type="email" ng-model="confirmEmail" name="confirm_email" class="form-control" placeholder="Retype Email" required>
			</div>
			<div class="form-group col-xs-12 col-sm-10">
				<input type="password" ng-model="password" name="password" class="form-control" placeholder="New Password" required>
			</div>
			<div class="form-group col-xs-12 col-sm-10">
				<input type="password" ng-model="confirmPassword" name="confirm_password" class="form-control" placeholder="Retype Password" required>
			</div>

			<div class="form-group col-xs-12 col-sm-10">
				<input type="text" ng-model="address" name="address" class="form-control" placeholder="Street Address" required>

			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input type="text" ng-model="city" name="city" class="form-control" placeholder="City" required>
			</div>
			<div class="form-group col-xs-12 col-sm-2">
				<!-- <input type="text" ng-model="state" name="state" class="form-control" placeholder="State" required> -->
				<select class="form-control" ng-model="state" ng-options="state.abbreviation for state in states" required>
				</select>
			</div>
			<div class="form-group col-xs-12 col-sm-2">
				<input type="text" ng-model="zipcode" name="zipcode" class="form-control" placeholder="Zipcode" required>
			</div>
			<div class="form-group col-xs-12 col-sm-10">
				<textarea ng-model="hours" rows="8" name="hours" class="form-control" placeholder="Hours of Operation" required>

				</textarea>
			</div>
			<div class="form-group col-xs-12 col-sm-10">
				<input type="url" ng-model="website" name="website" class="form-control" placeholder="http://www.yourwebsite.com" required>
			</div>
			<div class="form-group col-xs-12 col-sm-5">
				<input type="text" ng-model="contactName" name="contact_name" class="form-control" placeholder="Contact Name" required>
			</div>
			<div class="form-group col-xs-12 col-sm-5">
				<input type="text" ng-model="contactPhone" name="contact_phone" class="form-control" placeholder="Contact Phone #" required>
			</div>
			<div class="form-group col-xs-12 col-sm-10">
				<button class="btn btn-lg btn-primary btn-block">Submit</button>
			</div>
		</form>
	</div>

</div>
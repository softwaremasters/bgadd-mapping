<div class="row-fluid padding15">
    <div class="row-fluid mainMapContainer">
     	<div class="hidden-xs col-sm-3 col-md-2" style="padding-left: 20px; padding-top: 10px;">

       	<div class="layersSidebar">

          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              
              <a href="#collapse_POI" data-toggle="collapse" data-parent="#accordion">
                <h4>POI Layers</h4>
              </a>
              <div id="collapse_POI" class="panel-body panel-collapse collapse in">

                    <div ng-repeat="layer in layers" class="layer visible" ng-click="toggleVisiblityOfLayer(layer.id)">
                      <div class="visibleIndicator">
                        <i ng-show="layer.visible" class="fa fa-check-square"></i>
                      </div>
                      <img ng-src="{{layer.icon}}" class="legend">
                      <a class="name">{{layer.name}}</a>
                    </div>
                    <div class="layer visible" ng-click="hideAll()">
                      <div class="visibleIndicator"></div>
                      <a class="name">Hide All</a>
                    </div>
           
              </div>
            </div>
            <div class="panel panel-default">
              <a href="#collapse_thematic" data-toggle="collapse" data-parent="#accordion">
                <h4>Thematic Layers</h4>
              </a>
              <div id="collapse_thematic" class="panel-body panel-collapse collapse">

                <!-- inner collapsibles -->
                <div class="panel-group" id="accordion2">
                    
                    <div ng-repeat="layer in thematicLayers" class="panel layer visible">
                        <a href="#collapse-{{layer.id}}" data-toggle="collapse" data-parent="#accordion2" ng-click="toggleVisiblityOfThematicLayer(layer.id)" class="name-thematic">{{layer.name}}</a>
                        <div class="visibleIndicator">
                          <i ng-show="layer.visible" class="fa fa-check-square thematic-legend"></i>
                        </div>
                        <div id="collapse-{{layer.id}}" class="panel-collapse collapse">
                          <div class="panel-body">
                            <div ng-repeat="legend in layer.legend">
                              <img ng-src="{{legend.icon}}" class="thematic-legend">
                              <p class="label">{{legend.label}}</p>
                            </div>
                          </div>
                        </div>
                    </div>

                    <!-- hide all -->
                    <div class="panel">
                      <div class="layer visible">
                        <a href="#collapse-hideAll" data-toggle="collapse" 
                        data-parent="#accordion2" ng-click="hideAllThematicClicked()" class="name-thematic">Hide All</a>
                        <div id="collapse-hideAll" class="panel-collapse collapse">
                          <div class="panel-body">
                          </div>
                        </div>
                      </div>
                    </div>

                </div>
                <!-- /inner collapsibles -->

              </div>
            </div>
          </div>

        </div>
     	</div>
     	<div class="col-sm-9 col-md-10 fullheight mapcontainer" style="padding-left: 30px; padding-right: 0px;">
      	<div id="main_map">
          <div id="HomeButton"></div>
          <button class="advancedSearch" data-toggle="modal" data-target="#search"><i class="fa fa-search"></i></button>
          <select class="countyZoom" ng-model="kyCounty" ng-options="kyCounty.attributes.COUNTY for kyCounty in allKentuckyCounties" ng-click="zoomToCounty()">
            <option value="">Select a county...</option>
          </select>
        </div>
     	</div>
     	<div style="clear:both;"></div>

   </div>  <!-- mainMapContainer -->
</div> <!-- /container -->


<!-- Advanced Search Modal -->
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Advanced Search</h4>
      </div>
      <div class="modal-body">
        <!-- <select ng-model="layer" ng-options="layer.name for layer in layers" ng-change="setCounties()"> -->
        <select ng-model="layer" ng-options="layer.name for layer in layers" ng-change="setSearch()">
          <option value="" selected>-Select a layer-</option>
        </select>

        <div ng-show="layer">

          <div ng-show="industriesExist" class="ng-hide">
            <br>
            <select class="dynamic-select" ng-model="industry" ng-options="industry for industry in industries" ng-change="filterCounties(); filterSubcategories()">
              <option value="" selected>-Select an industry-</option>
            </select>
          </div>
          <div ng-show="retrievingIndustries">
            <i class="fa fa-spinner fa-spin"></i> Getting industries...
          </div>

          <div ng-show="subcategoriesExist" class="ng-hide">
            <br>
            <select class="dynamic-select" ng-model="subcategory" ng-options="subcategory for subcategory in subcategories" ng-change="filterCounties(); filterIndustries()">
              <option value="" selected>-Select a subcategory-</option>
            </select>
          </div>
          <div ng-show="retrievingSubcategories">
            <i class="fa fa-spinner fa-spin"></i> Getting subcategories...
          </div>

          <!-- <select ng-model="industry" ng-options="layer.name for layer in layers" ng-change="setCounties()">
            <option value="" selected>Select a layer...</option>
          </select> -->
          <h5><strong><u>Display Options</u></strong></h5>
          <div>
            <input type="radio" name="show_all" id="show_all" ng-model="searchOption" value="showAll"> 
            <label for="show_all" class="display-label">Show All </label></br>

            <input type="radio" name="click_radius" id="click_radius" ng-model="searchOption" value="clickRadius">
            <label for="click_radius" class="display-label">
              Within
              <select ng-model="mile" ng-options="mile for mile in miles" ng-click="selectClick()">
                <option value="" selected>5</option>
              </select>
              miles of clicked point on map
            </label>

            <!-- only show if geolocation has been enabled -->
            <div ng-show="currentLatitude">
              <input type="radio" name="currentLocation" id="currentLocation" ng-model="searchOption" value="currentLocation">
              <label for="currentLocation" class="display-label">
                Within
                <select ng-model="currentLocationMile" ng-options="currentLocationMile for currentLocationMile in currentLocationMiles" ng-click="selectCurrent()">
                  <option value="" selected>5</option>
                </select>
                miles of current location
              </label>
            </div>

            <div ng-show="countiesExist" class="ng-hide">
              <input type="radio" name="county" id="county" ng-model="searchOption" value="county"> 
              <label for="county" class="display-label">
                County
                <select ng-model="county" ng-options="county for county in counties" ng-click="selectCounty()">
                  <option value="">Select County</option>
                </select>
              </label>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="searchLayer()">Go</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  
  //check all possible fields when the last div is inserted
  $('body').on('DOMNodeInserted', 'div.website', function(){

      //get rid of any whitespaces
      var website = $("span.website");
      website.html($.trim(website.html()));
      
      var location = $("span.location");
      location.html($.trim(location.html()));

      var contact = $("span.contact-name");
      contact.html($.trim(contact.html()));
     
      var phone = $("span.contact-phone");
      phone.html($.trim(phone.html()));

      //hide if empty
      if(website.is(":empty")){
        $("div.website").css('display', 'none');
      }
      if(location.is(":empty")){
        $("div.location").css('display', 'none');
      }
      if(contact.is(":empty")){
        $("div.contact-name").css('display', 'none');
      }
      if(phone.is(":empty")){
        $("div.contact-phone").css('display', 'none');
      }

  });

</script>
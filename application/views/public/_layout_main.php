<!DOCTYPE html>
<html lang="en" ng-app="mappingApp">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>BGADD Mapping App</title>

    <script type="text/javascript" src="<?php echo base_url() ?>js/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/controllers.js"></script>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>css/custom-styles.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet">

    <!-- sidebar -->
    <link href="<?php echo base_url() ?>css/simple-sidebar.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

    <!-- ArcGIS Dependencies -->
    <link rel="stylesheet" href="http://js.arcgis.com/3.10/js/esri/css/esri.css">
    <script src="http://js.arcgis.com/3.10/"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
      // var x = document.getElementById("demo");
      
      //   if (navigator.geolocation) {
      //       navigator.geolocation.getCurrentPosition(function(position) {
      //         console.log(position);
      //       });
      //   } else {
      //       console.log("Geolocation not supported in this browser.");
      //   }
      

      // function showPosition(position) {
      //   console.log("Latitude: " + position.coords.latitude);
      //   console.log("Longitude: " + position.coords.longitude);
      // }
    </script>

  </head>

  <body>

    <?php if($main_content == 'public/index'): ?>
      <?php $this->load->view('assets/ajaxLoader'); ?>
    <?php endif; ?>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="pull-left nav-items">

        <?php if($main_content == 'public/index'): ?>
          <a href="#menu-toggle" id="menu-toggle"> <i class="fa fa-list"></i></a>
        <?php else: ?>
          <a href="<?php echo base_url() ?>" id="back-to-map"><i class="fa fa-globe"></i> Map</a>
        <?php endif; ?>

        <?php if($this->session->userdata('is_logged_in')): ?>
          <a href="<?php echo base_url() ?>login/logout" id="navigationButtons">Logout</a>

          <a href="<?php echo base_url() ?>farmersmarkets/dashboard" id="navigationButtons">Dashboard</a>

          <a href="<?php echo base_url() ?>farmersmarkets/dashboard/account" id="navigationButtons">Account</a>
        <?php else: ?>
          <a href="login" id="navigationButtons">Login</a>
        <?php endif; ?>
        
      </div>
      <div class="navbar-header pull-right">
        <a class="navbar-brand hidden-xs" href="<?php echo base_url()?>">KADIS Mapping App</a>
      </div>
    </div>

    <br><br>
    <?php $this->load->view($main_content); ?>

    <script>
      $(document).ready(function() {
        if(window.innerWidth < 768){
          $("#wrapper").toggleClass("toggled");
        }
        if(window.innerWidth >= 768 && !$("div#wrapper").hasClass("toggled")){
          $(".homeContainer").addClass("margin-250");
          $(".esriSimpleSlider").addClass("margin-250");
        }
        else{
          $(".homeContainer").removeClass("margin-250");
          $(".esriSimpleSlider").removeClass("margin-250");
        }
      });

      $(window).resize(function() {
        if(window.innerWidth >= 768 && !$("div#wrapper").hasClass("toggled")){
          $(".homeContainer").addClass("margin-250");
          $(".esriSimpleSlider").addClass("margin-250");
        }
        else{
          $(".homeContainer").removeClass("margin-250");
          $(".esriSimpleSlider").removeClass("margin-250");
        }
      });

      $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
          if(window.innerWidth < 768) {
            if(!$("#wrapper").hasClass("toggled")){
              $(".homeContainer").removeClass("margin-250");
              $(".esriSimpleSlider").removeClass("margin-250");
            }
          } else {
            if(!$("#wrapper").hasClass("toggled")){
              $(".homeContainer").addClass("margin-250");
              $(".esriSimpleSlider").addClass("margin-250");
            }
            else{
              $(".homeContainer").removeClass("margin-250");
              $(".esriSimpleSlider").removeClass("margin-250");
            }
          }
      });

    </script>

  </body>
</html>

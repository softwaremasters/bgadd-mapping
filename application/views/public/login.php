<div class="container padding15">

	<div class="loginInfo well">
		<div ng-controller="LoginController">
			<form ng-submit="verifyUser()">
				<h2 class="login">Login</h2>

				<p class="alert alert-danger ng-hide" ng-show="wrongLogin">{{ loginErrors }}</p>

				<div class="form-group">
					<input type="text" ng-model="username" name="username" class="form-control" placeholder="Username" required>
				</div>
				<div class="form-group">
					<input type="password" ng-model="password" name="password" class="form-control" placeholder="Password" required>
				</div>
				<div class="form-group">
					<input type="submit" value="Login" class="btn btn-lg btn-primary btn-block">
				</div>
			</form>

			<div>
				<small><a href="#" ng-click="forgotCredentials()">Forgot Username/Password</a></small>
				<div class="ng-hide" ng-show="forgotCreds">
					<form ng-submit="validateEmail()">

						<small>
							Enter the email address associated with your account.
						</small>

						<p class="alert alert-danger ng-hide" ng-show="wrongEmail">{{ validationMessage }}</p>
						<p class="alert alert-success ng-hide" ng-show="correctEmail">{{ validationMessage }}</p>

						<div class="form-group">
							<input type="email" ng-model="email" name="email" class="form-control" placeholder="Email" required>
						</div>

						<div class="form-group">
							<input type="submit" value="Send Reset Email" class="btn btn-lg btn-success btn-block">
						</div>
					</form>
				</div>
			</div>

		</div>

		<!-- <div class="col-xs-12 col-sm-6 well" ng-controller="SignUpController">
			<form ng-submit="signUpNewUser()">
				<h2 class="signup">Sign Up</h2>

				<p class="alert alert-danger ng-hide" ng-show="errors">{{ signUpErrors }}</p>

				<div class="form-group">
					<input type="text" ng-model="businessName" name="name" class="form-control" placeholder="Business Name" required>
				</div>
				<div class="form-group">
					<input type="text" ng-model="username" name="username" class="form-control" placeholder="Username" required>
				</div>
				<div class="form-group">
					<input type="email" ng-model="email" name="email" class="form-control" placeholder="Email" required>
				</div>
				<div class="form-group">
					<input type="email" ng-model="confirmEmail" name="confirm_email" class="form-control" placeholder="Retype Email" required>
				</div>
				<div class="form-group">
					<input type="password" ng-model="password" name="password" class="form-control" placeholder="Password" required>
				</div>
				<div class="form-group">
					<input type="password" ng-model="confirmPassword" name="confirm_password" class="form-control" placeholder="Retype Password" required>
				</div>
				<div class="form-group">
					<select class="form-control" ng-model="entityType" ng-options="entityType.type for entityType in entityTypes">
	                </select>
				</div>

				<div class="form-group">
					<button class="btn btn-lg btn-primary btn-block">Sign Up</button>
				</div>
			</form>
		</div> -->
	</div>

</div>
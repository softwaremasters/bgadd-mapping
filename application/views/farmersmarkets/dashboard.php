<div class="row-fluid">
	<h4 class="page-header">Basic Information:</h4>
	<div class="well col-xs-12">
		<div class="row basic-information">
			<div class="col-sm-4">
				<label>Address:</label>
				<p>
					{{entity.address}}
					<br>
					{{entity.city}}, {{entity.state}} {{entity.zipcode}}
				</p>
			</div>
			<div class="col-sm-4">
				<label>Contact Info:</label>
				<p>
					{{entity.contact_name}}
					<br>
					{{entity.contact_phone}}
				</p>
			</div>
			<div class="col-sm-4">
				<label>Hours of Operation:</label>
				<div ng-bind-html="entity.hours"></div>
			</div>
		</div>
		<div class="row basic-information">
			<div class="pull-right editBtn">
				<a href="#" ng-click="toggleEdit()">
					<i class="fa fa-edit"></i> 
					<span ng-hide="edittingBasic">Edit this information</span>
					<span ng-show="edittingBasic">Editing...</span>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid currentInventory">
	<div class="col-xs-6">
		<h4 class="page-header">Available Inventory:</h4>
		<div class="well nonInventoryItems">
			<div class="inventoryInstructions">
				<p>These are the available items that are not in your inventory. Click one to add it to your inventory.</p>
			</div>
			<div ng-repeat="produce in allProduce" class="produceItem" ng-click="addToInventory(produce)">
				{{produce.name}} <span ng-show="produce.type"> - {{produce.type}}</span>
				<span class="icon-right"><i class="fa fa-long-arrow-right"></i></span>
			</div>
		</div>
	</div>
	<div class="col-xs-6">
		<h4 class="page-header" ng-style="{'margin-left': '10px'}">Current Inventory:</h4>
		<div class="well inventoryItems">
			<div class="inventoryInstructions">
				<p>These are the items in your inventory. These items will be shown on the map to public users. They will be able to search for farmers markets with specified produce. If you have the specified produce in your current inventory, the user will find your market.</p>
			</div>
			<div class="row-fluid currentInventoryItem" ng-repeat="produce in currentInventory">
							
				<div class="produceItem">
					<div class="item" ng-click="removeFromInventory(produce)">
						{{produce.name}} <span ng-show="produce.type"> - {{produce.type}}</span>
						<span class="remove">
							<i class="fa fa-long-arrow-left"></i> Remove from inventory
						</span>
					</div>
				</div>
				<a class="btn-details" ng-click="showDetails(produce.id)">
					<i class="fa fa-info-circle"></i>
				</a>
				<div class="popoverContent" ng-show="produce.detailsVisibile">
					<input type="text" ng-model="produce.details" name="details" placeholder="Enter any details">
					<a class="btn btn-sm btn-primary" ng-click="saveDetails(produce)">
						<i class="fa fa-check"></i>
					</a>
					<a class="btn btn-sm btn-danger" ng-click="showDetails(produce.id)">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>

<div class="editAccountModal modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Account Settings</h4>
      		</div>
	      	<div class="modal-body">

		      	<p class="alert alert-danger ng-hide" ng-show="errors">{{errorMessage}}</p>

				<form ng-submit="saveAccountInfo()" class="editInfoForm">
					
					<div class="row">
						<div class="form-group col-xs-12">
							<label>Username</label>
							<input type="text" ng-model="username" name="username" class="form-control" placeholder="Username" required>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<label>Email</label>
							<input type="email" ng-model="email" name="email" class="form-control" placeholder="Email" email required>
						</div>
					</div>

					<div class="form-group col-md-6 col-md-offset-3">
						<button class="btn btn-md btn-inverse btn-block">Save Changes</button>
					</div>

				</form>
				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="editInfoModal modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Edit Information</h4>
      		</div>
	      	<div class="modal-body">
				<form ng-submit="saveNewInfo()" class="editInfoForm">
					<p class="alert alert-danger ng-hide col-xs-12" ng-show="basicErrors">{{ basicInfoErrors }}</p>

					
					<div class="row">
						<div class="form-group col-xs-12">
							<label>Business Name</label>
							<input type="text" ng-model="name" name="address" class="form-control" placeholder="Business Name" disabled>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<label>Address</label>
							<input type="text" ng-model="address" name="address" class="form-control" placeholder="Street Address" required>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-5">
							<label>City</label>
							<input type="text" ng-model="city" name="city" class="form-control" placeholder="City" required>
						</div>
						<div class="form-group col-xs-3">
							<label>State</label>
							<select class="form-control" ng-model="state" ng-options="state.abbreviation for state in states" required></select>
						</div>
						<div class="form-group col-xs-4">
							<label>Zip Code</label>
							<input type="text" ng-model="zipcode" name="zipcode" class="form-control" placeholder="Zipcode" required>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-6">
							<div>
								<label>Website</label>
								<div class="form-group">
									<input type="url" ng-model="website" name="website" class="form-control" placeholder="http://www.yourwebsite.com" required>
								</div>
							</div>
							<div>
								<label>Contact Name</label>
								<div class="form-group">
									<input type="text" ng-model="contactName" name="contact_name" class="form-control" placeholder="Contact Name" required>
								</div>
							</div>
							<div>
								<label>Contact Phone</label>
								<div class="form-group">
									<input type="text" ng-model="contactPhone" name="contact_phone" class="form-control" placeholder="Contact Phone #" required>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<label>Hours of Operation</label>
							<div class="form-group">
								<textarea ng-model="hours" rows="8" name="hours" class="form-control" placeholder="Hours of Operation" required ng-bind-html="hours"></textarea>
							</div>
						</div>
					</div>

					<div class="form-group col-md-6 col-md-offset-3">
						<button class="btn btn-md btn-inverse btn-block">Save Changes</button>
					</div>

				</form>
				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="resetPasswordModal modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Reset Password</h4>
      		</div>
	      	<div class="modal-body">

		      	<p class="alert alert-danger ng-hide" ng-show="errors">{{errorMessage}}</p>

				<form ng-submit="resetPassword()">
					<div class="form-group">
						<input type="password" ng-model="password" name="password" class="form-control" placeholder="New Password" required>
					</div>
					<div class="form-group">
						<input type="password" ng-model="confirmPassword" name="confirm_password" class="form-control" placeholder="Retype New Password" required>
					</div>
					<div class="form-group">
						<input type="submit" value="Change Password" class="btn btn-lg btn-primary btn-block">
					</div>
				</form>

				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
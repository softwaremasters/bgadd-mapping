<div class="container padding15">

	<div class="accountInfo well" ng-controller="AccountController">
		
		<h2 class="text-center">Account Information</h2>

			<p class="alert alert-danger ng-hide" ng-show="errors">{{errorMessage}}</p>
			<p class="alert alert-success ng-hide" ng-show="success">{{successMessage}}</p>

			<div class="padding10">
				<span class="accountInfo">Username</span>
				<span ng-hide="edittingUsername">{{user.username}}</span>

				<span ng-show="edittingUsername">
					<form ng-submit="saveUsername()" style="display: inline;">
						<input type="text" ng-model="username" name="username" placeholder="Username" required>
						<button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i></button>
					</form>
				</span>

				<span class="pull-right">
					<small>
						<a href="#" ng-click="toggleEditUsername()">
							<i class="fa fa-edit"></i>
							<span ng-hide="edittingUsername"> Edit</span>
							<span ng-show="edittingUsername"> Don't Edit</span>
						</a>
					</small>
				</span>
			</div>
			<div class="padding10">
				<span class="accountInfo">Email</span>
				<span ng-hide="edittingEmail">{{user.email}}</span>

				<span ng-show="edittingEmail">
					<form ng-submit="saveEmail()" style="display: inline;">
						<input type="email" ng-model="email" name="email" placeholder="Email" required>
						<button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i></button>
					</form>
				</span>

				<span class="pull-right">
					<small>
						<a href="#" ng-click="toggleEditEmail()">
							<i class="fa fa-edit"></i>
							<span ng-hide="edittingEmail"> Edit</span>
							<span ng-show="edittingEmail"> Don't Edit</span>
						</a>
					</small>
				</span>
			</div>

			<div class="padding10">
				<a href="#" class="btn btn-primary" ng-click="togglePasswordReset()">
					<span ng-hide="resettingPassword">Reset Password</span>
					<span ng-show="resettingPassword">Don't Reset Password</span>
				</a>
			</div>

			<div class="padding10" ng-show="resettingPassword">
				<form ng-submit="resetPassword()">
					<div class="form-group">
						<input type="password" ng-model="password" name="password" class="form-control" placeholder="New Password" required>
					</div>
					<div class="form-group">
						<input type="password" ng-model="confirmPassword" name="confirm_password" class="form-control" placeholder="Retype New Password" required>
					</div>
					<div class="form-group">
						<input type="submit" value="Change Password" class="btn btn-lg btn-primary btn-block">
					</div>
				</form>
			</div>
	</div>
</div>

<script type="text/javascript">
	$(".alert-success").on("show", function(){
		console.log('shown!');
		$("p.alert-success").fadeOut("slow");
	});
</script>
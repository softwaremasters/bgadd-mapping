<div class="row-fluid">
	
	<div class="col-xs-12">
		<h4 class="page-header">User Management</h4>
		<div class="row">
			<div class="col-sm-3">
				<button class="btn btn-md btn-inverse btn-block" ng-click="toggleAddUserModal()">Add New User</button>
			</div>
			<div class="col-sm-3">
				<a href="users" class="btn btn-md btn-inverse btn-block">View All Users</a>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="col-xs-12 col-sm-8">
		<div>
			<h4 class="page-header">Current Available Items</h4>
			<p class="font12">These are the available items that Farmer's Markets can choose from. Click the info icon to view and add new types.</p>
		</div>

		<div class="col-sm-4 addItemBtn">
			<button class="btn btn-md btn-inverse btn-block" ng-click="toggleAddNewItemModal()">Add New Item</button>
		</div>

		<table class="table table-striped table-hover table-condensed">
			<thead>
				<tr>
					<th>Item</th>
					<th>Info</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="produce in allProduce" class="produceItem">
					<td>{{produce.name}}</td>
					<td>
						<a class="btn-details btn-details-admin" ng-click="showItemInfo(produce)">
							<i class="fa fa-info-circle"></i>
						</a>
					</td>
					<td>
						<a class="btn-details btn-remove" ng-click="toggleRemoveModal(produce)">
							<i class="fa fa-times-circle"></i>
						</a>
					</td>
				</tr>
			</tbody>
		</table>

	</div>
</div>


<!-- ACCOUNT INFO MODAL -->
<div class="editAccountModal modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Account Settings</h4>
      		</div>
	      	<div class="modal-body">

		      	<p class="alert alert-danger ng-hide" ng-show="errors">{{errorMessage}}</p>

				<form ng-submit="saveAccountInfo()" class="editInfoForm">
					
					<div class="row">
						<div class="form-group col-xs-12">
							<label>Username</label>
							<input type="text" ng-model="username" name="username" class="form-control" placeholder="Username" required>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<label>Email</label>
							<input type="text" ng-model="email" name="email" class="form-control" placeholder="Email" email required>
						</div>
					</div>

					<div class="form-group col-md-6 col-md-offset-3">
						<button class="btn btn-md btn-inverse btn-block">Save Changes</button>
					</div>

				</form>
				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- ADD USER MODAL -->
<div class="addUserModal modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Add New User</h4>
      		</div>
	      	<div class="modal-body">
				<form ng-submit="saveNewUser()" class="editInfoForm">

					<p class="alert alert-danger ng-hide col-xs-12" ng-show="newUserErrors">{{ newUserErrorMessage }}</p>

					
					<div class="row">
						<div class="form-group col-xs-12">
							<label>Business Name</label>
							<input type="text" ng-model="newUserEntityName" name="name" class="form-control" placeholder="Business Name" required>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
							<label>KADIS ID</label>
							<p class="font12">*This will be used to generate the new user's initial username and password</p>
							<input type="text" ng-model="newUserKadisID" name="newUserKadisID" class="form-control" placeholder="RX-79BD-1" required>
						</div>
					</div>

					<div class="form-group col-md-6 col-md-offset-3">
						<button class="btn btn-md btn-inverse btn-block">Submit</button>
					</div>

				</form>
				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ADD ITEM MODAL -->
<div class="addItemModal modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Add New Item</h4>
      		</div>
	      	<div class="modal-body">
				<form ng-submit="saveNewItem()">

					<p class="alert alert-danger ng-hide col-xs-12" ng-show="newItemErrors">{{ newItemErrorMessage }}</p>

					<div class="row">
						<div class="form-group col-xs-12">
							<input type="text" ng-model="newItem" name="name" class="form-control" placeholder="Item Name" required>
						</div>
					</div>

					<div class="form-group col-md-6 col-md-offset-3">
						<button class="btn btn-md btn-inverse btn-block">Submit</button>
					</div>

				</form>
				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- REMOVE ITEM MODAL -->
<div class="removeItemModal modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Remove {{ itemToRemove.name }} </h4>
      		</div>
	      	<div class="modal-body">

				<p class="alert alert-danger col-xs-12">
					You're about to delete {{ itemToRemove.name }}. <br />
					Deleting {{ itemToRemove.name }} will remove it from {{ numberOfMarketsWithItem }} Farmer's Market's inventories and remove {{ numberOfItemTypes }} types associated with it. Do you want to continue?
				</p>

				<div class="alert alert-danger" ng-show="removeItemErrors">{{ removeItemMessage }}</div>

				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		        	<button class="btn btn-danger btn-md" ng-click="removeItem(itemToRemove)">Delete</button>
		      	</div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ITEM INFO MODAL -->
<div class="itemInfoModal modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">{{ currentItem.name }}</h4>
      		</div>
	      	<div class="modal-body">

	      		<div ng-hide="typesExist">
	      			No types yet.
	      		</div>

	      		<div ng-show="typesExist">
		      		<h5>Types:</h5>
		      		<ul>
			      		<li ng-repeat="currentItemType in currentItemTypes">
			      			{{ currentItemType.name }}
			      		</li>
			      	</ul>
			    </div>

			    <p class="alert alert-danger ng-hide col-xs-12" ng-show="newItemTypeErrors">{{ newItemTypeErrorMessage }}</p >

				<form ng-submit="saveNewItemType(currentItem.id)">

					<div class="padding-left-20">
						<input type="text" ng-model="newItemType" name="newItemType" placeholder="Enter a new type" required>
						<button class="btn btn-sm btn-primary"><i class="fa fa-check"></i></button>
					</div>

				</form>

				<div class="form-group col-md-3 col-md-offset-9">
					<button class="btn btn-md btn-inverse btn-block" ng-click="hideItemInfoModal()">Done</button>
				</div>

				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="resetPasswordModal modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        		<h4 class="modal-title">Reset Password</h4>
      		</div>
	      	<div class="modal-body">

		      	<p class="alert alert-danger ng-hide" ng-show="errors">{{errorMessage}}</p>

				<form ng-submit="resetPassword()">
					<div class="form-group">
						<input type="password" ng-model="password" name="password" class="form-control" placeholder="New Password" required>
					</div>
					<div class="form-group">
						<input type="password" ng-model="confirmPassword" name="confirm_password" class="form-control" placeholder="Retype New Password" required>
					</div>
					<div class="form-group">
						<input type="submit" value="Change Password" class="btn btn-lg btn-primary btn-block">
					</div>
				</form>

				<div style="clear:both;"></div>
	      	</div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
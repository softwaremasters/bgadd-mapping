<!DOCTYPE html>
<html lang="en" ng-app="mappingApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title>BGADD Mapping App</title>

        <script type="text/javascript" src="<?php echo base_url() ?>js/angular.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/controllers.js"></script>

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url() ?>css/custom-styles.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url() ?>css/admin.css" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet">

        <!-- sidebar -->
        <link href="<?php echo base_url() ?>css/simple-sidebar.css" rel="stylesheet">

        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

        <!-- ArcGIS Dependencies -->
        <link rel="stylesheet" href="http://js.arcgis.com/3.10/js/esri/css/esri.css">
        <script src="http://js.arcgis.com/3.10/"></script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <?php
      $last = $this->uri->total_segments();
      $page = $this->uri->segment($last);
    ?>
    <?php if($page == 'dashboard'): ?>
      <body ng-controller="AdminDashboardController">
    <?php else: ?>
      <body ng-controller="AdminUserController">
    <?php endif; ?>


    <nav class="navbar navbar-default" role="navigation">
      <div class="row-fluid">
          <ul class="nav navbar-nav col-xs-4">
            <li class="map-link">
                <a href="<?php echo base_url() ?>" id="back-to-map" target="_blank"><i class="fa fa-globe fa-2x"></i> <span>View Map</span></a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right col-xs-8 pull-right">
            <li class="dropdown pull-right">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url() ?>admin/dashboard" id="navigationButtons">Dashboard</a></li>
                <li><a ng-click="toggleAccountSettings()">Account Settings</a></li>
                <li><a ng-click="togglePasswordReset()">Change Password</a></li>
                <li><a href="<?php echo base_url() ?>login/logout" id="navigationButtons">Logout</a></li>
              </ul>
            </li>
          </ul>
      </div><!-- /.container-fluid -->
    </nav>

    <section class="main container">
        <h2 class="page-header">Admin Dashboard</h2>
        <?php $this->load->view($main_content); ?>
    </section>

    <footer class="footer">
      <div class="container">
        <div class="pull-left" ng-style="{'margin-top':'12px'}">
          &copy; <?php echo date('Y'); ?> Bluegrass Area Development District - All Rights Reserved
        </div>
        <div class="pull-right" ng-style="{'text-align':'right'}">
          <a href="http://www.softwaremasters.com">
            <img src="http://www.softwaremasters.com/img/logo.png" ng-style="{'width':'80px'}">
          </a>
        </div>
      </div>
    </footer>

    <script>
      $(document).ready(function() {
        if(window.innerWidth < 768){
          $("#wrapper").toggleClass("toggled");
        }
        if(window.innerWidth >= 768 && !$("div#wrapper").hasClass("toggled"))
          $(".mainMapContainer").addClass("padding-250");
        else
          $(".mainMapContainer").removeClass("padding-250");
      });

      $(window).resize(function() {
        if(window.innerWidth >= 768 && !$("div#wrapper").hasClass("toggled"))
          $(".mainMapContainer").addClass("padding-250");
        else
          $(".mainMapContainer").removeClass("padding-250");
      });

      $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
          if(window.innerWidth < 768) {
            if(!$("#wrapper").hasClass("toggled"))
              $(".mainMapContainer").removeClass("padding-250");
          } else {
            if(!$("#wrapper").hasClass("toggled"))
              $(".mainMapContainer").addClass("padding-250");
            else
              $(".mainMapContainer").removeClass("padding-250");
          }
      });
    </script>

  </body>
</html>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entities_model extends MY_Model {
	protected $_table_name = 'entities';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

}

/* End of file */
/* Location: ./application/models/ */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entity_types_model extends MY_Model {
	protected $_table_name = 'entity_types';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

}

/* End of file */
/* Location: ./application/models/ */
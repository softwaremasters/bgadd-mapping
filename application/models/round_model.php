<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Round_model extends MY_Model {
	protected $_table_name = 'rounds';
	protected $_primary_key = 'round_ID';
	protected $_order_by = 'round_ID';

}

/* End of file */
/* Location: ./application/models/ */
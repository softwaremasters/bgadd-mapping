<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_entries_model extends MY_Model {
	protected $_table_name = 'inventory_entries';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

}

/* End of file */
/* Location: ./application/models/ */
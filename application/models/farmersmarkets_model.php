<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Farmersmarkets_model extends MY_Model {
	protected $_table_name = 'farmersmarkets';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

}

/* End of file */
/* Location: ./application/models/ */
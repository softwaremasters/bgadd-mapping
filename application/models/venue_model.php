<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_model extends MY_Model {
	protected $_table_name = 'venues';
	protected $_primary_key = 'venue_ID';
	protected $_order_by = 'venue_ID';

}

/* End of file */
/* Location: ./application/models/ */
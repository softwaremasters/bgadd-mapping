<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_title'] = "BGADD";
		$this->load->model('users_model');
		$this->load->model('entities_model');
		$this->load->model('entity_types_model');
		$this->load->model('entity_inventories_model');
		$this->load->model('inventory_entries_model');
		$this->load->model('produce_model');
		$this->load->model('produce_types_model');

		$this->load->helper('typography');
	}

	public function index(){

		$this->data['main_content'] = 'admin/dashboard';
		$this->load->view('admin/assets/_layout_main', $this->data);
	}

	public function get_entity_types(){
		$entity_types = $this->entity_types_model->get();
		echo json_encode($entity_types);
	}

	public function get_user_info(){
		$user_id = $this->session->userdata('user_id');
		$user = $this->users_model->get($user_id);
		echo json_encode($user);
	}

	public function get_entity_info(){
		$entity_id = $this->session->userdata('entity_id');
		$entity = $this->entities_model->get($entity_id);
		
		$entity->hours = nl2br_except_pre($entity->hours);

		$entity_type = $this->entity_types_model->get($entity->entity_type);
		$entity->type = $entity_type->type;

		echo json_encode($entity);
	}

	public function get_all_produce(){

		$this->db->order_by('name', 'asc');
		$produce = $this->produce_model->get();

		$this->db->order_by('name', 'asc');
		$types = $this->produce_types_model->get();

		//sort by name
		usort($produce, function($a, $b){
			return strcmp($a->name, $b->name);
		});

		echo json_encode($produce);
	}

	//get all the types of the previously passed produce/item id
	public function get_types(){
		$produce_id = $this->input->post('produce_id');

		$types = $this->produce_types_model->get_by(array('produce_id' => $produce_id));

		if(!empty($types)){
			echo json_encode($types);
		} else {
			echo '';
			die();
		}

	}

	//add a new item to the list
	public function add_new_item(){
		$data = $this->input->post();

		$can_insert = $this->check_if_item_exists($data['name']);

		if($can_insert){
			if($this->produce_model->save($data)){
				echo 'success';
			} else {
				echo 'failed';
			}
		} else {
			echo 'exists';
		}		
	}

	//add new item type
	public function add_new_type(){
		$data = $this->input->post();

		$can_insert = $this->check_if_type_exists($data);

		if($can_insert){
			if($this->produce_types_model->save($data)){
				echo 'success';
			} else {
				echo 'failed';
			}
		} else {
			echo 'exists';
		}
	}

	//delete an item and all associated types from the db
	//along with db entries containing it
	public function delete_item(){
		$produce_id = $this->input->post('produce_id');

		$entries = $this->entity_inventories_model->get_by(array('produce' => $produce_id));
		$types = $this->produce_types_model->get_by(array('produce_id' => $produce_id));

		if(!empty($entries)){
			$this->db->where('produce', $produce_id);
			if(!$this->db->delete('entity_inventories')){
				echo 'failed entries'; die();
			}
		}

		if(!empty($types)){
			$this->db->where('produce_id', $produce_id);
			if(!$this->db->delete('produce_types')){
				echo 'failed type'; die();
			}
		}

		$this->produce_model->delete($produce_id);
		echo 'success';

	}

	//pass in a new user based on kadis id and then
	//create an entity for them with a name
	public function add_new_user(){
		$data = $this->input->post();

		$kadis_id = $data['kadis_id'];

		if($this->check_kadis_is_unique($kadis_id)){
			$entity_data['name'] = $data['name'];
			$entity_data['entity_type'] = 1;
			$entity_data['kadis_id'] = $kadis_id;

			$entity_id = $this->entities_model->save($entity_data);

			if($entity_id){

				$user_data['username'] = $kadis_id;
				$user_data['entity_id'] = $entity_id;
				$user_data['password'] = md5($kadis_id);
				$user_data['role'] = 2;

				if($this->users_model->save($user_data)){
					echo 'success';
				} else {
					echo 'failed';
				}

			} else {
				echo 'failed';
			}
		} else {
			echo 'not unique';
		}

	}

	public function get_number_of_markets_with_item($produce_id){

		$this->db->distinct();
		$this->db->select('inventory_entry_id');
		$entity_inventories_with_item = $this->entity_inventories_model->get_by(array('produce' => $produce_id));

		echo count($entity_inventories_with_item);

	}

	public function get_number_of_item_types($produce_id){

		$types = $this->produce_types_model->get_by(array('produce_id' => $produce_id));
		echo count($types);

	}

	public function account(){
		$this->data['main_content'] = 'admin/account';
		$this->load->view('admin/assets/_layout_main', $this->data);
	}

	public function save_username(){
		$data['username'] = $this->input->post('username');

		if(!$this->check_username_availability($data['username'])){
			echo 'Username in use';
		} else {
			$user_id = $this->session->userdata('user_id');
			$this->users_model->save($data, $user_id);

			$this->session->set_userdata('username', $data['username']);

			echo 'saved';
		}
	}

	public function save_email_address(){
		$data['email'] = $this->input->post('email');
		$user_id = $this->session->userdata('user_id');

		if($this->users_model->save($data, $user_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

	public function check_kadis_is_unique($kadis_id){
		$kadis_check = $this->entities_model->get_by(array('kadis_id' => $kadis_id));

		if(empty($kadis_check)){
			return true;
		}
		return false;
	}

	public function check_username_availability($username){
	//Check if the user clicked save without changing their name. If they did, just return true.
		
		if($username != $this->session->userdata('username')){
			$user = $this->users_model->get_by(array('username' => $username));
			return empty($user);
		}

		return true;
	}

	public function check_if_type_exists($data){
		$types = $this->produce_types_model->get_by(array('produce_id' => $data['produce_id']));

		if(empty($types)){
			return true;
		} else {
			foreach($types as $type){
				if(strtolower($type->name) == strtolower($data['name'])){
					return false;
				}
			}
			return true;
		}
	}

	public function check_if_item_exists($item_name){
		$items = $this->produce_model->get();

		foreach($items as $item){
			if(strtolower($item->name) == strtolower($item_name)){
				return false;
			}
		}
		return true;
	}

	public function reset_password(){
		$data['password'] = md5($this->input->post('password'));
		$user_id = $this->session->userdata('user_id');

		if($this->users_model->save($data, $user_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

}
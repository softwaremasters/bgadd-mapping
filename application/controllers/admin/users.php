<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_title'] = "BGADD";
		$this->load->model('users_model');
		$this->load->model('entities_model');
		$this->load->model('entity_types_model');
		$this->load->model('entity_inventories_model');
		$this->load->model('inventory_entries_model');
		$this->load->model('produce_model');
		$this->load->model('produce_types_model');

		$this->load->helper('typography');
	}

	public function index(){

		$this->data['main_content'] = 'admin/users';
		$this->load->view('admin/assets/_layout_main', $this->data);
	}

	public function get_entity_types(){
		$entity_types = $this->entity_types_model->get();
		echo json_encode($entity_types);
	}

	public function get_user_info(){
		$user_id = $this->session->userdata('user_id');
		$user = $this->users_model->get($user_id);
		echo json_encode($user);
	}

	public function get_entity_info(){
		$entity_id = $this->session->userdata('entity_id');
		$entity = $this->entities_model->get($entity_id);
		
		$entity->hours = nl2br_except_pre($entity->hours);

		$entity_type = $this->entity_types_model->get($entity->entity_type);
		$entity->type = $entity_type->type;

		echo json_encode($entity);
	}

	public function get_all_users(){

		//entity
		$role = 2;

		$this->db->order_by('username', 'asc');
		$users = $this->users_model->get_by(array('role' => $role));

		foreach($users as $user){
			$user->entity = $this->entities_model->get($user->entity_id);
		}

		echo json_encode($users);
	}

	//pass in a new user based on kadis id and then
	//create an entity for them with a name
	public function add_new_user(){
		$data = $this->input->post();

		$kadis_id = $data['kadis_id'];

		if($this->check_kadis_is_unique($kadis_id)){
			$entity_data['name'] = $data['name'];
			$entity_data['entity_type'] = 1;
			$entity_data['kadis_id'] = $kadis_id;

			$entity_id = $this->entities_model->save($entity_data);

			if($entity_id){

				$user_data['username'] = $kadis_id;
				$user_data['entity_id'] = $entity_id;
				$user_data['password'] = md5($kadis_id);
				$user_data['role'] = 2;

				if($this->users_model->save($user_data)){
					echo 'success';
				} else {
					echo 'failed';
				}

			} else {
				echo 'failed';
			}
		} else {
			echo 'not unique';
		}

	}

	//admin can reset a user's password
	public function reset_user_password(){
		$data['password'] = md5($this->input->post('password'));
		$user_id = $this->input->post('user_id');

		$this->users_model->save($data, $user_id);

		echo 'saved';
	}

	public function delete_user(){
		$user_id = $this->input->post('user_id');

		$user = $this->users_model->get($user_id);
		$entity = $this->entities_model->get($user->entity_id);

		$inventory_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity->id));

		if(!empty($inventory_entry)){
			$inventory = $this->entity_inventories_model->get_by(array('inventory_entry_id' => $inventory_entry[0]->id));

			if(!empty($inventory)){
				$this->db->where('inventory_entry_id', $inventory_entry[0]->id);
				$this->db->delete('entity_inventories');
			}

			$this->inventory_entries_model->delete($inventory_entry[0]->id);
		}

		$this->entities_model->delete($user->entity_id);
		$this->users_model->delete($user_id);

		echo 'success';
	}


	public function check_kadis_is_unique($kadis_id){
		$kadis_check = $this->entities_model->get_by(array('kadis_id' => $kadis_id));

		if(empty($kadis_check)){
			return true;
		}
		return false;
	}

	public function save_username(){
		$data['username'] = $this->input->post('username');

		if(!$this->check_username_availability($data['username'])){
			echo 'Username in use';
		} else {
			$user_id = $this->session->userdata('user_id');
			$this->users_model->save($data, $user_id);

			$this->session->set_userdata('username', $data['username']);

			echo 'saved';
		}
	}

	public function save_email_address(){
		$data['email'] = $this->input->post('email');
		$user_id = $this->session->userdata('user_id');

		if($this->users_model->save($data, $user_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

	public function check_username_availability($username){
	//Check if the user clicked save without changing their name. If they did, just return true.
		
		if($username != $this->session->userdata('username')){
			$user = $this->users_model->get_by(array('username' => $username));
			return empty($user);
		}

		return true;
	}

	public function reset_password(){
		$data['password'] = md5($this->input->post('password'));
		$user_id = $this->session->userdata('user_id');

		if($this->users_model->save($data, $user_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

}
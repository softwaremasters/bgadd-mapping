<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_title'] = "BGADD";
		$this->load->model('users_model');
		$this->load->model('entities_model');
		$this->load->model('entity_types_model');
		$this->load->model('inventory_entries_model');
	}

	public function index(){
		
		if($this->session->userdata('is_logged_in')){
			if($this->session->userdata('role') > 1){
				redirect('farmersmarkets/dashboard');	
			} else {
				redirect('admin/dashboard');
			}
			
		}

		$this->data['main_content'] = 'public/login';
		$this->load->view('public/_layout_main', $this->data);
	}

	public function get_entity_types(){
		$entity_types = $this->entity_types_model->get();
		echo json_encode($entity_types);
	}

	public function verify_user(){
		$data = $this->input->post();
		$username = $data['username'];
		$password = $data['password'];

		$password = md5($password);
		$user = $this->users_model->get_by(array('username' => $username, 'password' => $password));

		if($user){

			$data = array(
				'username' => $user[0]->username, 
				'email' => $user[0]->email, 
				'user_id' => $user[0]->id, 
				'entity_id' => $user[0]->entity_id,
				'role' => $user[0]->role,
				'is_logged_in' => TRUE
			);

			$this->session->set_userdata($data);

			if($user[0]->role == 1){
				echo 'admin';
			} else {
				echo 'true';	
			}
		}

		else{
			echo 'false';
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

	public function sign_up(){

		$entity_data['entity_type'] = $this->input->post('entity_type')['id'];
		$entity_data['name'] = $this->input->post('name');

		$user_data['username'] = $this->input->post('username');
		$user_data['password'] = md5($this->input->post('password'));
		$user_data['email'] = $this->input->post('email');
		
		if(!$this->check_username_availability($user_data['username'])){
			echo 'Username in use';
		} else {

			$user_data['entity_id'] = $this->entities_model->save($entity_data);

			if(!empty($user_data['entity_id'])){
				$user_id = $this->users_model->save($user_data);

				$new_inventory_entry['entity_id'] = $user_data['entity_id'];
				$this->inventory_entries_model->save($new_inventory_entry);

				if(!empty($user_id)){

					$data = array(
						'username' => $user_data['username'], 
						'email' => $user_data['email'], 
						'user_id' => $user_id, 
						'entity_id' => $user_data['entity_id'],
						'is_logged_in' => TRUE
					);

					$this->session->set_userdata($data);

					echo 'success';
				}
				else{
					echo 'failed';
				}
			}
			else{
				echo 'failed';
			}
		}

	}

	public function check_username_availability($username){
		$user = $this->users_model->get_by(array('username' => $username));
		return empty($user);
	}

	public function basic_info(){
		$user_id = $this->session->userdata('user_id');
		$user = $this->users_model->get($user_id);
		$entity = $this->entities_model->get($user->entity_id);
		$entity->type = $this->entity_types_model->get($entity->entity_type);

		/*if the user has already completed their information, 
		don't let them come back here. They can change this
		info from the dashboard.*/

		if(!empty($entity->address)){
			redirect('login');
		}

		$this->data['user'] = $user;
		$this->data['entity'] = $entity;
		$this->data['main_content'] = 'public/basic_information';

		$this->load->view('public/_layout_main', $this->data);
	}

	public function save_basic_info(){

		$entity_id = $this->session->userdata('entity_id');
		$user_id = $this->session->userdata('user_id');

		$user_data = $this->users_model->array_from_post(array('password', 'email'));
		$user_data['password'] = md5($user_data['password']);

		$entity_data = $this->entities_model->array_from_post(
			array(
				'address',
				'city',
				'state',
				'zipcode',
				'hours',
				'website',
				'contact_name',
				'contact_phone'
			)
		);

		$entity_data['entity_type'] = 1;

		if($this->users_model->save($user_data, $user_id)){
			if($this->entities_model->save($entity_data, $entity_id)){

				$new_inventory_entry['entity_id'] = $entity_id;

				if($this->inventory_entries_model->save($new_inventory_entry)){
					echo 'success';
				} else {
					echo 'failed';
				}
			}
			else{
				echo 'failed';
			}
		} else {
			echo 'failed';
		}

	}

	public function forgot_email(){

		$email = $this->input->post('email');
		$user = $this->users_model->get_by(array('email' => $email));
		$user = $user[0];

		if($user){
			$this->load->library('email');
			$new_password = $this->get_random_string();

			$data['password'] = md5($new_password);
			$this->users_model->save($data, $user->id);

			$this->email->from('support@bgadd.com', 'BGADD');
			$this->email->to($email);
			$this->email->subject('Your BGADD Login Credentials');
			$message = "Here are your login credentials for BGADD.\r\n\r\n";
			$message .= "Your username is: ".$user->username."\r\n";
			$message .= "Your new temporary password is: ".$new_password."\r\n\r\n";
			$message .= "You can change your new password at any time after logging in at ".base_url('login').".";
			$this->email->message($message);
			$this->email->send();

			echo $message;

			echo 'true';

		}
		else{
			echo 'false';
		}

	}

	public function get_random_string(){
		$random_number = rand(8, 12);
		$valid_characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$random_string = '';

		for($i = 0; $i < $random_number; $i++){
			$random_string .= $valid_characters[mt_rand(0, strlen($valid_characters))];
		}

		return $random_string;
	}

	public function get_base_url(){
		$this->load->helper('url');
		echo base_url();
	}

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_title'] = "BGADD";
		$this->load->model('users_model');
		$this->load->model('entities_model');
		$this->load->model('entity_types_model');
		$this->load->model('entity_inventories_model');
		$this->load->model('inventory_entries_model');
		$this->load->model('produce_model');
		$this->load->model('produce_types_model');

		$this->load->helper('typography');
		
		$this->has_completed_basic_info();
	}

	public function index(){
		$this->data['main_content'] = 'farmersmarkets/dashboard';
		$this->load->view('farmersmarkets/assets/_layout_main', $this->data);
	}

	public function has_completed_basic_info(){

		$user_id = $this->session->userdata('user_id');
		$user = $this->users_model->get($user_id);

		$entity_id = $this->session->userdata('entity_id');
		$entity = $this->entities_model->get($entity_id);

		if(empty($entity->address)){
			redirect('login/basic_info');
		}
	}

	public function get_entity_types(){
		$entity_types = $this->entity_types_model->get();
		echo json_encode($entity_types);
	}

	public function get_user_info(){
		$user_id = $this->session->userdata('user_id');
		$user = $this->users_model->get($user_id);
		echo json_encode($user);
	}

	public function get_entity_info(){
		$entity_id = $this->session->userdata('entity_id');
		$entity = $this->entities_model->get($entity_id);
		
		$entity->hours = nl2br_except_pre($entity->hours);

		$entity_type = $this->entity_types_model->get($entity->entity_type);
		$entity->type = $entity_type->type;

		echo json_encode($entity);
	}

	public function get_current_inventory(){
		$entity_id = $this->session->userdata('entity_id');

		// $this->db->order_by('last_updated', 'desc');
		$latest_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity_id))[0];
		$inventory_details = $this->entity_inventories_model->get_by(array('inventory_entry_id' => $latest_entry->id));

		foreach($inventory_details as $inventory_detail){
			$produce = $this->produce_model->get($inventory_detail->produce);
			
			if(!empty($inventory_detail->produce_type)){
				$produce_type = $this->produce_types_model->get($inventory_detail->produce_type);
				$inventory_detail->type = $produce_type->name;
			}
			else{
				$inventory_detail->type = null;
			}
			
			$inventory_detail->name = $produce->name;
		}

		//sort by name and type
		usort($inventory_details, function($a, $b){
			$name_check = strcmp($a->name, $b->name);
			if($name_check == 0){
				return strcmp($a->type, $b->type);
			}
			else{
				return $name_check;
			}
		});

		echo json_encode($inventory_details);
	}

	public function get_all_produce(){

		$this->db->order_by('name', 'asc');
		$produce = $this->produce_model->get();

		$this->db->order_by('name', 'asc');
		$types = $this->produce_types_model->get();

		foreach($types as $type){
			$this_produce = $this->produce_model->get($type->produce_id);
			$this_produce->type = $type->name;
			array_push($produce, $this_produce);
		}

		foreach($produce as $produce_item){
			if(!isset($produce_item->type)){
				$produce_item->type = null;
			}
		}

		//sort by name and type
		usort($produce, function($a, $b){
			$name_check = strcmp($a->name, $b->name);
			if($name_check == 0){
				return strcmp($a->type, $b->type);
			}
			else{
				return $name_check;
			}
		});

		echo json_encode($produce);
	}

	public function get_last_updated_on(){
		$entity_id = $this->session->userdata('entity_id');
		$inventory_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity_id))[0];

		if(!empty($inventory_entry->last_updated)){
			$last_updated = date('m/d/Y g:i:s a', strtotime($inventory_entry->last_updated));
		} else {
			$last_updated = 'Never';
		}

		echo $last_updated;
	}

	public function edit_basic_info(){
		$user_id = $this->session->userdata('user_id');
		$user = $this->users_model->get($user_id);

		$entity = $this->entities_model->get($user->entity_id);
		$entity->type = $this->entity_types_model->get($entity->entity_type);

		$data = $this->input->post();
		$data['entity_type'] = $entity->entity_type;

		if($this->entities_model->save($data, $entity->id)){
			echo 'success';
		}
		else{
			echo 'failed';
		}

	}

	public function add_to_inventory(){
		$data = $this->input->post();

		$produce = $this->produce_model->get_by(array('name' => $data['name']))[0];

		if(!empty($data['type'])){
			$produce_type = $this->produce_types_model->get_by(array('name' => $data['type']))[0];
			$produce_type = $produce_type->id;
		}

		$entity_id = $this->session->userdata('entity_id');
		$entity = $this->entities_model->get($entity_id);

		$inventory_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity_id))[0];

		$inventory_data['inventory_entry_id'] = $inventory_entry->id;
		$inventory_data['produce'] = $produce->id;

		if(isset($produce_type)){
			$inventory_data['produce_type'] = $produce_type;
		} else{
			$inventory_data['produce_type'] = null;
		}
		
		if(isset($data['details'])){
			$inventory_data['details'] = $data['details'];
		} else{
			$inventory_data['details'] = null;
		}

		if($this->entity_inventories_model->save($inventory_data)){
			$inventory_entry_data['last_updated'] = date('Y-m-d H:i:s', time());

			$this->inventory_entries_model->save($inventory_entry_data, $inventory_entry->id);

			echo 'saved';
		} else {
			echo 'failed';
		}

	}

	public function remove_from_inventory(){
		$entity_inventory_id = $this->input->post('entity_inventory_id');

		$entity_id = $this->session->userdata('entity_id');

		$inventory_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity_id))[0];
		$inventory_entry_data['last_updated'] = date('Y-m-d H:i:s', time());

		$this->entity_inventories_model->delete($entity_inventory_id);
		$this->inventory_entries_model->save($inventory_entry_data, $inventory_entry->id);

		echo 'removed';
	}

	public function save_details(){
		$entity_inventory_id = $this->input->post('entity_inventory_id');
		$data['details'] = $this->input->post('details');

		if($this->entity_inventories_model->save($data, $entity_inventory_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

	public function account(){
		$this->data['main_content'] = 'farmersmarkets/account';
		$this->load->view('farmersmarkets/assets/_layout_main', $this->data);
	}

	public function save_username(){
		$data['username'] = $this->input->post('username');

		if(!$this->check_username_availability($data['username'])){
			echo 'Username in use';
		} else {
			$user_id = $this->session->userdata('user_id');
			$this->users_model->save($data, $user_id);

			$this->session->set_userdata('username', $data['username']);

			echo 'saved';
		}
	}

	public function save_email_address(){
		$data['email'] = $this->input->post('email');
		$user_id = $this->session->userdata('user_id');

		if($this->users_model->save($data, $user_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

	public function check_username_availability($username){

		/*
		Check if the user clicked save without changing their name.
		If they did, just return true.
		*/
		
		if($username != $this->session->userdata('username')){
			$user = $this->users_model->get_by(array('username' => $username));
			return empty($user);
		}

		return true;
	}

	public function reset_password(){
		$data['password'] = md5($this->input->post('password'));
		$user_id = $this->session->userdata('user_id');

		if($this->users_model->save($data, $user_id)){
			echo 'saved';
		} else {
			echo 'failed';
		}
	}

}
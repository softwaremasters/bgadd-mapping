<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('entities_model');
		$this->load->model('inventory_entries_model');
		$this->load->model('entity_inventories_model');
		$this->load->model('produce_model');
		$this->load->model('produce_types_model');

		$this->load->model('farmersmarkets_model');
	}

	public function index() {
		$this->data['main_content'] = 'public/index';
		$this->load->view('public/_layout_main', $this->data);
	}

	public function get_all_farmers_markets(){
		$farmers_markets = $this->entities_model->get();
		echo json_encode($farmers_markets); 
	}

	public function get_farmers_market_inventory(){
		$kadis_id = $this->input->post('kadis_id');

		$entity = $this->entities_model->get_by(array('kadis_id' => $kadis_id))[0];
		$entity_id = $entity->id;

		$latest_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity_id));

		if(!empty($latest_entry)){
			$latest_entry = $latest_entry[0];
		} else {
			echo 'none';
			die();
		}

		$inventory_details = $this->entity_inventories_model->get_by(array('inventory_entry_id' => $latest_entry->id));

		foreach($inventory_details as $inventory_detail){
			$produce = $this->produce_model->get($inventory_detail->produce);
			
			if(!empty($inventory_detail->produce_type)){
				$produce_type = $this->produce_types_model->get($inventory_detail->produce_type);
				$inventory_detail->type = $produce_type->name;
			}
			else{
				$inventory_detail->type = null;
			}
			
			$inventory_detail->name = $produce->name;
		}

		//sort by name and type
		usort($inventory_details, function($a, $b){
			$name_check = strcmp($a->name, $b->name);
			if($name_check == 0){
				return strcmp($a->type, $b->type);
			}
			else{
				return $name_check;
			}
		});

		echo json_encode($inventory_details);
	}

	public function get_last_updated_on(){
		$kadis_id = $this->input->post('kadis_id');

		$entity = $this->entities_model->get_by(array('kadis_id' => $kadis_id))[0];
		$entity_id = $entity->id;

		$latest_entry = $this->inventory_entries_model->get_by(array('entity_id' => $entity_id));

		if(!empty($latest_entry)){
			$latest_entry = $latest_entry[0];
		}

		if(!empty($latest_entry->last_updated)){
			$last_updated = date('m/d/Y g:i:s a', strtotime($latest_entry->last_updated));
		} else {
			$last_updated = 'Never';
		}

		echo $last_updated;
	}

	/*	this will return the kadis_id of farmer's markets that 
		have whatever item and/or type passed to this function  */		
	public function get_farmers_markets_by_inventory(){
		$item_info = $this->input->post('item_info');
		$item_info = $item_info[0];

		$kadis_array = array();

		if(isset($item_info['produce_id'])){
			//id here is the id of the type (pulled from the db)
			$where = array('produce' => $item_info['produce_id'], 'produce_type' => $item_info['id']);
		}
		else{
			//id is produce id (pulled from db)
			$where = array('produce' => $item_info['id']);
		}

		$entity_inventories_with_product = $this->entity_inventories_model->get_by($where);

		foreach($entity_inventories_with_product as $entity_inventory){
			$entity_entry = $this->inventory_entries_model->get($entity_inventory->inventory_entry_id);
			$entity = $this->entities_model->get($entity_entry->entity_id);
			array_push($kadis_array, $entity->kadis_id);
		}
		
		$kadis_array = array_unique($kadis_array);
		$kadis_array = array_filter($kadis_array);
		$kadis_array = array_values($kadis_array);

		echo json_encode($kadis_array);
	}

	public function get_farmers_market_items(){
		$this->db->order_by('name', 'asc');
		$produce = $this->produce_model->get();

		echo json_encode($produce);
	}

	public function get_farmers_market_item_types(){
		$item_id = $this->input->post('item');

		$this->db->order_by('name', 'asc');
		$types = $this->produce_types_model->get_by(array('produce_id' => $item_id));

		if(!empty($types)){
			echo json_encode($types);	
		} else {
			echo 'empty';
		}
		
	}


	// public function save_farmersmarkets(){
	// 	$farmers_markets = $this->input->post('farmers_markets');

	// 	foreach($farmers_markets as $market){
	// 		$entity_data['name'] = $market['name'];
	// 		$entity_data['kadis_id'] = $market['kadis_id'];
	// 		$entity_data['entity_type'] = 1;

	// 		$entity_id = $this->entities_model->save($entity_data);

	// 		$user_data['username'] = $market['kadis_id'];
	// 		$user_data['password'] = md5($market['kadis_id']);
	// 		$user_data['entity_id'] = $entity_id;

	// 		$this->users_model->save($user_data);
	// 	}

	// 	echo 'success';
	// }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
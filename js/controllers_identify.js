var mappingApp = angular.module('mappingApp', []);
    
    /*=============
    MAIN CONTROLLER
    =============*/
    mappingApp.controller('MainController', ['$scope', '$http', '$compile', '$window', function($scope, $http, $compile, $window) {
        
        $('.mapcontainer').css('opacity', '0.0');
        
        require(["esri/map",  
                "esri/layers/ArcGISTiledMapServiceLayer", 
                "esri/layers/DynamicLayerInfo", 
                "esri/dijit/Legend",
                "esri/dijit/Popup", 
                "dojo/_base/array", 
                "esri/tasks/IdentifyTask", 
                "esri/tasks/query", 
                "esri/tasks/QueryTask", 
                "esri/tasks/GeometryService", 
                "esri/geometry/Circle",  
                "esri/geometry/Point", 
                "esri/geometry/Extent", 
                "esri/geometry/Polygon", 
                "esri/geometry/webMercatorUtils", 
                "esri/tasks/ProjectParameters", 
                "esri/tasks/IdentifyParameters", 
                "esri/layers/FeatureLayer", 
                "esri/dijit/InfoWindowLite", 
                "esri/dijit/HomeButton", 
                "esri/InfoTemplate", 
                "esri/symbols/SimpleMarkerSymbol", 
                "esri/symbols/SimpleLineSymbol", 
                "esri/renderers/SimpleRenderer", 
                "esri/symbols/SimpleFillSymbol", 
                "esri/tasks/query", 
                "esri/SpatialReference", 
                "esri/request", 
                "dojo/on", 
                "dojo/dom-construct", 
                "dojo/domReady!", 
                "esri/Color", 
                "esri/graphic", 
                "esri/tasks/FindTask", 
                "esri/tasks/FindParameters"],

            function(Map, Tiled, DynamicLayerInfo, Legend, Popup, arrayUtils, Scalebar, Query, GeometryService, Point, Extent, Polygon, webMercatorUtils, ProjectParameters, FeatureLayer, InfoWindowLite, HomeButton, InfoTemplate, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, SimpleRenderer, SpatialReference, esriRequest, domConstruct, Color, Graphic, FindTask, FindParameters) {
                
                $scope.map = new Map("main_map", {
                    center: [-86, 37.6],
                    slider: true,
                    sliderOrientation: "vertical",
                    zoom: 7
                });

                var identifyTask, identifyParams;

                $scope.home = new esri.dijit.HomeButton({
                    map: $scope.map
                }, "HomeButton");
                $scope.home.startup();

                $scope.map.on("load", function(e) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        $scope.currentLatitude = position.coords.latitude;
                        $scope.currentLongitude = position.coords.longitude;
                        $scope.addCurrentLocationToMap();
                    });
                });

                //showAll by default
                $scope.searchOption = "showAll";
                $scope.miles = [15, 30, 45, 60, 75, 90, 105, 120];
                $scope.currentLocationMiles = [15, 30, 45, 60, 75, 90, 105, 120];
                $scope.zipcode = "";
                $scope.counties = [];
                $scope.industries = [];
                $scope.subcategories = [];
                $scope.items = [];
                $scope.itemTypes = [];
                $scope.retrievingItems = false;
                $scope.retrievingItemTypes = false;
                $scope.retrievingIndustries = false;
                $scope.retrievingSubcategories = false;

                // var basemap = new esri.layers.ArcGISTiledMapServiceLayer("http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer");
                var basemap = new esri.layers.ArcGISTiledMapServiceLayer("http://kygisserver.ky.gov/arcgis/rest/services/WGS84WM_Services/Ky_TCM_Base_WGS84WM/MapServer");
                $scope.map.addLayer(basemap);
                
                //easier to change it once
                $scope.serviceURL = "http://maps2.bgadd.org/arcgis/rest/services/KADIS/KADIS_POI/MapServer";
                $scope.thematicURL = "http://maps2.bgadd.org/arcgis/rest/services/KADIS/KADIS_Thematic/MapServer";

                $scope.kadis_layer = new esri.layers.ArcGISDynamicMapServiceLayer($scope.serviceURL);
                $scope.kadis_thematic_layer = new esri.layers.ArcGISDynamicMapServiceLayer($scope.thematicURL);
                $scope.kadis_thematic_layer.setOpacity(0.5);

                $scope.map.addLayer($scope.kadis_layer);
                $scope.map.addLayer($scope.kadis_thematic_layer);


                $scope.map.on("load", mapReady);

                function mapReady () {
                    $scope.map.on("click", executeIdentifyTask);
                    //create identify tasks and setup parameters
                    identifyTask = new esri.tasks.IdentifyTask("http://maps2.bgadd.org/arcgis/rest/services/KADIS/KADIS_POI/MapServer");
                    
                    identifyParams = new esri.tasks.IdentifyParameters();
                    identifyParams.tolerance = 5;
                    identifyParams.returnGeometry = true;

                    identifyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_VISIBLE;
                    identifyParams.width = $scope.map.width;
                    identifyParams.height = $scope.map.height;
                }

                function executeIdentifyTask (event) {

                    identifyParams.layerIds = [];

                    //only show results for visible layers
                    for(i in $scope.layers){
                        if($scope.layers[i].visible){
                            identifyParams.layerIds.push($scope.layers[i].id);
                        }
                    }

                    //check to see if we have extra padding still
                    var mapDiv = document.getElementsByClassName('mainMapContainer');
                    var mapDiv = angular.element(mapDiv);
                    
                    var mapPoint = event.mapPoint;
                    // console.log(mapPoint);
                    //if we do, get the screen points, remove 250 from x, and convert back to map coordinates
                    if(mapDiv[0].className.indexOf('padding-250') > -1){     
                        var screenPoint = $scope.map.toScreen(mapPoint);
                        screenPoint.x = screenPoint.x - 250;
                        var mapPoint = $scope.map.toMap(screenPoint);
                    }

                    // console.log(mapPoint);
                    identifyParams.geometry = mapPoint;
                    identifyParams.mapExtent = $scope.map.extent;

                    // console.log(identifyParams);

                    var deferred = identifyTask
                        .execute(identifyParams)
                        .addCallback(function (response) {

                            return arrayUtils.map(response, function (result) {
                                var feature = result.feature;

                                layerName = result.layerName;
                                                    
                                feature.attributes.layerName = layerName;
                                var infoTemplate = new esri.InfoTemplate();

                                switch(layerName) {
                                    case 'Farmers Markets': 

                                        var title = "<div id='${KADIS_ID}' class='farmersMarketTitle'><b>Farmers Market</b></div>";
                                        var content = "<div class='farmersMarketContent btn btn-primary btn-block' data-toggle='modal' data-target='#viewInventory' ng-click='viewInventory()'></div>";

                                        //compile here so that ng-click works
                                        content = $compile(content)($scope)[0];

                                        infoTemplate.setTitle(title);
                                        infoTemplate.setContent(content);
                                        feature.setInfoTemplate(infoTemplate);
                                        break;

                                    case 'Kentucky Proud': 

                                        infoTemplate.setTitle("<b>${COMPANY}</b>");
                                        var content = "<div class='website website-wrap'><i class='fa fa-external-link-square'></i>: <a href='${WEB}' target='_blank'><span class='website'>${WEB}</span></a><br/></div>";
                                        infoTemplate.setContent(content);
                                        feature.setInfoTemplate(infoTemplate);
                                        break;

                                    default:

                                        infoTemplate.setTitle("<b>${COMPANY}</b>");
                                        var content = "<div class='type'><b>Type</b>: <span class='type'>${INDUSTRY}</span></div><div class='location'><i class='fa fa-map-marker'></i>: <span class='location'>${ADDRESS}<br/> ${CITY}, ${STATE} ${ZIP}</span></div><div class='contact-name'><i class='fa fa-user'></i>: <span class='contact-name'>${CONT_FIRST} ${CONT_LAST}</span></div><div class='contact-phone'><i class='fa fa-phone'></i>: <span class='contact-phone'>${PHONE}</span></div><div class='website website-wrap'><i class='fa fa-external-link-square'></i>: <a href='${WEB}' target='_blank'><span class='website'>${WEB}</span></a></div>";
                                        infoTemplate.setContent(content);
                                        feature.setInfoTemplate(infoTemplate);
                                        break;
                                }

                                return feature;
                        });
                        
                    });
                    
                    $scope.map.infoWindow.show(event.mapPoint);
                    $scope.map.infoWindow.setFeatures([deferred]);
                }

                $scope.layers = [];
                $scope.thematicLayers = [];
                $scope.visibleLayers = [];
                $scope.makeVisible = [];
                $scope.makeVisibleThematic = [];

                //thematic layer
                $scope.kadis_thematic_layer.on("load", function(e) {
                    $scope.dynamicLayerInfos = e.target.createDynamicLayerInfosFromLayerInfos();
                    
                    arrayUtils.forEach($scope.dynamicLayerInfos, function(info) {
                        var i = { 
                            id: info.id,
                            name: info.name,
                            position: info.id
                        };
                        i.visible = $scope.kadis_thematic_layer.layerInfos[info.id].defaultVisibility;
                        i.name = i.name.replace(/_/g, ' ');
                        $scope.thematicLayers.push(i);

                    });
                    
                    $scope.kadis_thematic_layer.setVisibleLayers([-1]);

                    //make sure this is on the bottom
                    $scope.map.reorderLayer($scope.kadis_thematic_layer, 1);

                    $scope.$apply();
                });

                //POI layer
                $scope.kadis_layer.on("load", function(e) {
                    $scope.dynamicLayerInfos = e.target.createDynamicLayerInfosFromLayerInfos();
                    
                    arrayUtils.forEach($scope.dynamicLayerInfos, function(info) {
                        var i = { 
                            id: info.id,
                            name: info.name,
                            position: info.id
                        };
                        i.visible = $scope.kadis_layer.layerInfos[info.id].defaultVisibility;
                        i.name = i.name.replace(/_/g, ' ');
                        $scope.layers.push(i);

                    });

                    $scope.kadis_layer.setVisibleLayers([-1]);
                    dojo.connect($scope.kadis_layer, "onUpdateEnd", $scope.initialLoadComplete);

                    if(window.innerWidth > 767){
                        $("#wrapper").toggleClass("toggled");
                        $(".mainMapContainer").addClass("padding-250");
                    }

                    $scope.$apply();
                });

            }
        );

        $scope.toggleVisiblityOfLayer = function(layerid) {
            
            $('#pleaseWaitDialog').show();

            // $scope.hideAllThematic();
            if($scope.featureLayer){
                $scope.map.removeLayer($scope.featureLayer);
            }

            if(window.innerWidth < 768){
                $("#wrapper").toggleClass("toggled");
            }

            $scope.searchOption = 'showAll';

            $scope.map.graphics.clear();
            $scope.addCurrentLocationToMap();

            if($scope.layers[layerid].visible) {
                $scope.layers[layerid].visible = false;
                $scope.makeVisible.pop(layerid);
            } else {
                $scope.layers[layerid].visible = true;
                $scope.makeVisible.push(layerid);
            }

            console.log($scope.makeVisible);

            if ($scope.makeVisible.length > 0) {
                $scope.kadis_layer.setVisibleLayers($scope.makeVisible);
            } else {
                $scope.kadis_layer.setVisibleLayers($scope.makeVisible);
            }

            $('#pleaseWaitDialog').hide();
        }

        function getAllFarmersMarkets(){

            $http.get('index/get_all_farmers_markets').success(function(data) { 
                $scope.farmersMarkets = data;
                $window.farmersMarkets = $scope.farmersMarkets;
            });
        }

        function getAllKadisFarmersMarkets(){
            var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/0');
            var query = new esri.tasks.Query();
            query.where = "1=1";
            query.returnGeometry = true;
            query.orderByFields = ["MarketName"];
            query.outFields = ["*"];

            queryTask.execute(query, function(results){

                $scope.kadisFarmersMarkets = results.features;

                $scope.$apply();
            });
        }

        // function saveAllFarmersMarkets(){

        //     var farmersmarketsArray = [];

        //     for(i in $scope.kadisFarmersMarkets){
        //         var data = {
        //             "name": $scope.kadisFarmersMarkets[i].attributes.MarketName,
        //             "kadis_id": $scope.kadisFarmersMarkets[i].attributes.KADIS_ID
        //         };
        //         farmersmarketsArray.push(data);
        //     }

        //     console.log(farmersmarketsArray);

        //     $.ajax({
        //         type: "POST", 
        //         url: "index/save_farmersmarkets",
        //         data: {"farmers_markets": farmersmarketsArray},
        //         success: function(response) {
        //             if(response == 'success'){
        //                 console.log('Saved!');
        //             }
        //             else{
        //                 console.log('Failed!');
        //             }
        //         }
        //     });
        // }

        $scope.toggleVisiblityOfThematicLayer = function(layerid) {
            $('#pleaseWaitDialog').show();

            // $scope.hideAll();
            // if($scope.featureLayer){
            //     $scope.map.removeLayer($scope.featureLayer);
            // }
            if($scope.countyLayer){
                $scope.map.removeLayer($scope.countyLayer);
            }

            if(window.innerWidth < 768){
                $("#wrapper").toggleClass("toggled");
            }

            $scope.searchOption = 'showAll';
            
            $scope.map.graphics.clear();
            $scope.addCurrentLocationToMap();
            
            if($scope.thematicLayers[layerid].visible) {
                $scope.thematicLayers[layerid].visible = false;
                $scope.makeVisibleThematic.pop(layerid);
            } else {
                $scope.hideAllThematic();
                $scope.thematicLayers[layerid].visible = true;
                $scope.makeVisibleThematic.push(layerid);
            }

            // console.log($scope.makeVisibleThematic);

            if ($scope.makeVisibleThematic.length > 0) {
                $scope.kadis_thematic_layer.setVisibleLayers($scope.makeVisibleThematic);
            } else {
                // $scope.makeVisibleThematic.push(-1);
                $scope.kadis_thematic_layer.setVisibleLayers($scope.makeVisibleThematic);
            }

            //reorder the layers on the map so that thematic layers don't cover the POI layers
            // $scope.map.reorderLayer($scope.thematicLayers[layerid].featureLayer, 0);

            $('#pleaseWaitDialog').hide();
        }

        $scope.selectClick = function(){
            $scope.searchOption = "clickRadius";
        }
        $scope.selectCurrent = function(){
            $scope.searchOption = "currentLocation";
        }
        $scope.selectCounty = function(){
            $scope.searchOption = "county";
        }

        $scope.layerUpdateCompleted = function(error) {
            $('#pleaseWaitDialog').hide();
            if (error) {
                alert("An error has occured: ", error);
            }
        }

        $scope.initialLoadComplete = function(error) {

            get_POI_icons();
            get_thematic_icons();
            getAllKentuckyCounties();
            getAllFarmersMarkets();
            getAllKadisFarmersMarkets();
            
            $('#pleaseWaitDialog').hide();
            $('.mapcontainer').css('opacity', '1.0');

            console.log($scope);
        }

        $scope.hideAll = function(error) {

            for(var j = 0; j < $scope.layers.length; j++) {
                // $scope.layers[j].featureLayer.hide();
                $scope.layers[j].visible = false;
                $scope.makeVisible.pop(j);
            }

            $scope.kadis_layer.setVisibleLayers($scope.makeVisible);
        }

        $scope.hideAllThematic = function(error) {
            for(var j = 0; j < $scope.thematicLayers.length; j++) {
                // $scope.thematicLayers[j].featureLayer.hide();
                $scope.thematicLayers[j].visible = false;
                $scope.makeVisibleThematic.pop(j);
            }

            $scope.kadis_thematic_layer.setVisibleLayers($scope.makeVisibleThematic);
        }

        //for when the user clicks "hide all" in the thematic menu
        $scope.hideAllThematicClicked = function(error) {
            for(var j = 0; j < $scope.thematicLayers.length; j++) {
                // $scope.thematicLayers[j].featureLayer.hide();
                $scope.makeVisibleThematic.pop(j);
                $scope.thematicLayers[j].visible = false;

                $("#collapse-"+j).collapse({
                    toggle: false
                })

                $("#collapse-"+j).collapse('hide');
            }

            $scope.kadis_thematic_layer.setVisibleLayers($scope.makeVisibleThematic);

            $("#collapse-hideAll").collapse('show');
        }

        $scope.addCurrentLocationToMap = function() {
            var point = new esri.geometry.Point($scope.currentLongitude, $scope.currentLatitude);
            var symbol = new esri.symbol.PictureMarkerSymbol({
                "angle" : 0,
                "xoffset" : 2,
                "yoffset" : 8,
                "type" : "esriPMS",
                "url" : "http://static.arcgis.com/images/Symbols/Basic/RedShinyPin.png",
                "contentType" : "image/png",
                "width" : 24,
                "height" : 24
            });
            $scope.map.graphics.add( new esri.Graphic(point, symbol) );
        }

        $scope.setSearch = function(){
            $scope.setCounties();

            if($scope.layer.id > 0){
                $scope.retrievingItems = false;
                $scope.farmersMarketItemsExist = false;
                $scope.farmersMarketItemTypesExist = false;

                $scope.item = null;
                $scope.itemType = null;

                $scope.setSubcategories();
                $scope.setIndustries();
            }
            else {
                $scope.industriesExist = false;
                $scope.subcategoriesExist = false;
                $scope.industry = null;
                $scope.subcategory = null;
                $scope.setFarmersMarketItems();
            }
            
        }

        $scope.setFarmersMarketItems = function(){
            $scope.retrievingItems = true;
            $scope.farmersMarketItemsExist = false;

            $http.get('index/get_farmers_market_items').success(function(data){
                console.log(data);
                $scope.farmersMarketItems = data;

                if($scope.farmersMarketItems.length > 0){
                    $scope.farmersMarketItemsExist = true;
                }
                $scope.retrievingItems = false;
            });
        }

        $scope.setFarmersMarketItemTypes = function(){

            if($scope.item == null){
                $scope.itemTypes = "";
                $scope.itemType = "";
                $scope.retrievingItemTypes = false;
                $scope.farmersMarketItemTypesExist = false;
            }
            else{
                $scope.retrievingItemTypes = true;
                $scope.farmersMarketItemTypesExist = false;

                $.ajax({
                    type: "POST", 
                    url: "index/get_farmers_market_item_types",
                    data:{
                        "item": $scope.item.id
                    },
                    success: function(response) {
                        if(response == 'empty'){
                            $scope.itemTypes = "";
                            $scope.retrievingItemTypes = false;
                            $scope.$apply();
                        }
                        else{
                            $scope.itemTypes = JSON.parse(response);
                            $scope.farmersMarketItemTypesExist = true;
                            $scope.retrievingItemTypes = false;
                            $scope.$apply();
                        }
                    }
                });
            }
        }

        $scope.filterCountiesByItem = function(){
            //send item to controller, look at the entities that have
            //the item in their inventory, relate those entities
            //by kadis_id to the kadisFarmersMarket array in $scope
            //since the KADIS mapserver has the county
            if($scope.item != null){

                $scope.counties = [];
                $scope.countiesExist = false;

                var itemInformation = [];

                if($scope.itemType){
                    itemInformation.push($scope.itemType);
                } else {
                     itemInformation.push($scope.item);
                }

                $scope.kadisIDsWithItems = null;

                $.ajax({
                    type: "POST", 
                    url: "index/get_farmers_markets_by_inventory",
                    data:{
                        "item_info": itemInformation
                    },
                    success: function(response) {
                        $scope.kadisIDsWithItems = JSON.parse(response);
                        if($scope.kadisIDsWithItems.length > 0){
                            for(var i = 0; i < $scope.kadisIDsWithItems.length; i++){
                                console.log($scope.kadisIDsWithItems[i]);
                                for(j in $scope.kadisFarmersMarkets){
                                    if($scope.kadisIDsWithItems[i] == $scope.kadisFarmersMarkets[j].attributes.KADIS_ID){
                                        $scope.counties.push($scope.kadisFarmersMarkets[j].attributes.County);
                                    }
                                }
                            }
                            console.log($scope.counties);
                            $scope.countiesExist = true;
                        }
                        else{
                            $scope.kadisIDsWithItems = null;
                            $scope.counties = [];
                            $scope.countiesExist = false;
                        }
                        $scope.$apply();
                    }
                });
            }
            else{
                $scope.kadisIDsWithItems = null;
                $scope.setCounties();
            }
        }

        $scope.setSubcategories = function(){

            if($scope.layer == null){
                $scope.subcategories = [];
            }
            else if($scope.layer.id == 0 || $scope.layer.id == 12){
                $scope.subcategories = [];
                $scope.subcategoriesExist = false;
            }
            else{
                $scope.retrievingSubcategories = true;
                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "1=1";
                query.returnGeometry = true;
                if($scope.layer.id == 8){
                    query.orderByFields = ["SUBCATEGORY"];
                    query.outFields = ["SUBCATEGORY"];    
                }
                else{
                    query.orderByFields = ["SUBCATEGOR"];
                    query.outFields = ["SUBCATEGOR"];
                }

                queryTask.execute(query, function(results){
                    // console.log(results);
                    $scope.subcategoriesExist = false;
                    $scope.subcategories = [];
                    for(var i = 1; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            continue;
                        }
                        else if($scope.layer.id == 8){
                            if(i == 0 && results.features[i].attributes.SUBCATEGORY != null && results.features[i].attributes.SUBCATEGORY != " "){
                                $scope.subcategories.push(results.features[i].attributes.SUBCATEGORY);
                            }
                            else if(results.features[i].attributes.SUBCATEGORY != null && results.features[i].attributes.SUBCATEGORY != " "
                            && results.features[i-1].attributes.SUBCATEGORY != null && results.features[i-1].attributes.SUBCATEGORY != " "){
                                if(results.features[i-1].attributes.SUBCATEGORY.toLowerCase() != results.features[i].attributes.SUBCATEGORY.toLowerCase()){
                                    $scope.subcategories.push(results.features[i].attributes.SUBCATEGORY);
                                }
                            }
                        }
                        else{
                            if(i == 0 && results.features[i].attributes.SUBCATEGOR != null && results.features[i].attributes.SUBCATEGOR != " "){
                                $scope.subcategories.push(results.features[i].attributes.SUBCATEGOR);
                            }
                            else if(results.features[i].attributes.SUBCATEGOR != null && results.features[i].attributes.SUBCATEGOR != " "
                            && results.features[i-1].attributes.SUBCATEGOR != null && results.features[i-1].attributes.SUBCATEGOR != " "){
                                if(results.features[i-1].attributes.SUBCATEGOR.toLowerCase() != results.features[i].attributes.SUBCATEGOR.toLowerCase()){
                                    $scope.subcategories.push(results.features[i].attributes.SUBCATEGOR);
                                }
                            }
                        }
                    }
                    if($scope.subcategories.length > 0){
                        $scope.subcategoriesExist = true;
                    }
                    $scope.subcategories = $.unique($scope.subcategories);
                    $scope.retrievingSubcategories = false;
                    $scope.$apply();
                });
            }
        }

        $scope.setIndustries = function(){

            if($scope.layer == null){
                $scope.industries = [];
            }
            else if($scope.layer.id == 0 || $scope.layer.id == 12){
                $scope.industries = [];
                $scope.industriesExist = false;
            }
            else{
                $scope.retrievingIndustries = true;
                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "1=1";
                query.returnGeometry = true;
                query.orderByFields = ["INDUSTRY"];
                query.outFields = ["INDUSTRY"];

                queryTask.execute(query, function(results){
                    // console.log(results);
                    $scope.industriesExist = false;
                    $scope.industries = [];
                    for(var i = 1; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            continue;
                        }
                        else{
                            if(i == 0 && results.features[i].attributes.INDUSTRY != null && results.features[i].attributes.INDUSTRY != " "){
                                $scope.industries.push(results.features[i].attributes.INDUSTRY);
                            }
                            else if(results.features[i].attributes.INDUSTRY != null && results.features[i].attributes.INDUSTRY != " "
                            && results.features[i-1].attributes.INDUSTRY != null && results.features[i-1].attributes.INDUSTRY != " "){
                                if(results.features[i-1].attributes.INDUSTRY.toLowerCase() != results.features[i].attributes.INDUSTRY.toLowerCase()){
                                    $scope.industries.push(results.features[i].attributes.INDUSTRY);
                                }
                            }
                        }
                    }
                    if($scope.industries.length > 0){
                        $scope.industriesExist = true;
                    }
                    $scope.industries = $.unique($scope.industries);
                    $scope.retrievingIndustries = false;
                    $scope.$apply();
                });
            }
        }

        $scope.filterSubcategories = function(){

            if($scope.industry == null){
                $scope.setSubcategories();
            }
            else if($scope.layer == null){
                $scope.subcategories = [];
            }
            else if($scope.layer.id == 0 || $scope.layer.id == 12){
                $scope.subcategories = [];
                $scope.subcategoriesExist = false;
            }
            else{
                $scope.retrievingSubcategories = true;
                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "INDUSTRY = '" +$scope.industry+ "'";
                query.returnGeometry = true;
                if($scope.layer.id == 8){
                    query.orderByFields = ["SUBCATEGORY"];
                    query.outFields = ["SUBCATEGORY"];    
                }
                else{
                    query.orderByFields = ["SUBCATEGOR"];
                    query.outFields = ["SUBCATEGOR"];
                }

                queryTask.execute(query, function(results){
                    $scope.subcategoriesExist = false;
                    $scope.subcategories = [];
                    for(var i = 0; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            continue;
                        }
                        else if($scope.layer.id == 8){
                            if(i == 0 && results.features[i].attributes.SUBCATEGORY != null && results.features[i].attributes.SUBCATEGORY != " "){
                                $scope.subcategories.push(results.features[i].attributes.SUBCATEGORY);
                            }
                            else if(results.features[i].attributes.SUBCATEGORY != null && results.features[i].attributes.SUBCATEGORY != " "
                            && results.features[i-1].attributes.SUBCATEGORY != null && results.features[i-1].attributes.SUBCATEGORY != " "){
                                if(results.features[i-1].attributes.SUBCATEGORY.toLowerCase() != results.features[i].attributes.SUBCATEGORY.toLowerCase()){
                                    $scope.subcategories.push(results.features[i].attributes.SUBCATEGORY);
                                }
                            }
                        }
                        else{
                            if(i == 0 && results.features[i].attributes.SUBCATEGOR != null && results.features[i].attributes.SUBCATEGOR != " "){
                                $scope.subcategories.push(results.features[i].attributes.SUBCATEGOR);
                            }
                            else if(results.features[i].attributes.SUBCATEGOR != null && results.features[i].attributes.SUBCATEGOR != " "
                            && results.features[i-1].attributes.SUBCATEGOR != null && results.features[i-1].attributes.SUBCATEGOR != " "){
                                if(results.features[i-1].attributes.SUBCATEGOR.toLowerCase() != results.features[i].attributes.SUBCATEGOR.toLowerCase()){
                                    $scope.subcategories.push(results.features[i].attributes.SUBCATEGOR);
                                }
                            }
                        }
                    }
                    if($scope.subcategories.length > 0){
                        $scope.subcategoriesExist = true;
                    }
                    $scope.subcategories = $.unique($scope.subcategories);
                    $scope.retrievingSubcategories = false;
                    $scope.$apply();
                });
            }

        }

        $scope.filterIndustries = function(){

            if($scope.subcategory == null){
                $scope.setIndustries();
            }
            else if($scope.layer == null){
                $scope.industries = [];
            }
            else if($scope.layer.id == 0 || $scope.layer.id == 12){
                $scope.industries = [];
                $scope.industriesExist = false;
            }
            else{
                $scope.retrievingIndustries = true;
                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                if($scope.layer.id == 8){
                    query.where = "SUBCATEGORY = '" +$scope.subcategory+ "'";
                }
                else{
                    query.where = "SUBCATEGOR = '" +$scope.subcategory+ "'";
                }
                query.returnGeometry = true;
                query.orderByFields = ["INDUSTRY"];
                query.outFields = ["INDUSTRY"];

                queryTask.execute(query, function(results){
                    $scope.industriesExist = false;
                    $scope.industries = [];
                    for(var i = 0; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            continue;
                        }
                        if(i == 0 && results.features[i].attributes.INDUSTRY != null && results.features[i].attributes.INDUSTRY != " "){
                            $scope.industries.push(results.features[i].attributes.INDUSTRY);
                        }
                        else{
                            if(results.features[i].attributes.INDUSTRY != null && results.features[i].attributes.INDUSTRY != " "
                            && results.features[i-1].attributes.INDUSTRY != null && results.features[i-1].attributes.INDUSTRY != " "){
                                if(results.features[i-1].attributes.INDUSTRY.toLowerCase() != results.features[i].attributes.INDUSTRY.toLowerCase()){
                                    $scope.industries.push(results.features[i].attributes.INDUSTRY);
                                }
                            }
                        }
                    }
                    if($scope.industries.length > 0){
                        $scope.industriesExist = true;
                    }
                    $scope.industries = $.unique($scope.industries);
                    $scope.retrievingIndustries = false;
                    $scope.$apply();
                });
            }
        }

        $scope.filterCounties = function(){

            if($scope.subcategory != null && $scope.industry != null){
                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "SUBCATEGOR = '" +$scope.subcategory +"' AND INDUSTRY = '" +$scope.industry +"'";
                query.returnGeometry = true;
                query.orderByFields = ["COUNTY"];
                query.outFields = ["COUNTY"];

                queryTask.execute(query, function(results){
                    $scope.countiesExist = false;
                    $scope.counties = [];
                    for(var i = 1; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            if(results.features[i].attributes.County != null && results.features[i].attributes.County != " "
                            && results.features[i-1].attributes.County != null && results.features[i-1].attributes.County != " "){
                                if(results.features[i-1].attributes.County.toLowerCase() != results.features[i].attributes.County.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.County);
                                }
                            }
                        }
                        else{
                           if(results.features[i].attributes.COUNTY != null && results.features[i].attributes.COUNTY != " "
                            && results.features[i-1].attributes.COUNTY != null && results.features[i-1].attributes.COUNTY != " "){
                                if(results.features[i-1].attributes.COUNTY.toLowerCase() != results.features[i].attributes.COUNTY.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.COUNTY);
                                }
                            }
                        }
                    }
                    if($scope.counties.length > 0){
                        $scope.countiesExist = true;
                    }
                    $scope.counties = $.unique($scope.counties);

                    // console.log("AFTER: ");
                    // console.log($scope.counties);
                    $scope.$apply();
                });
            }
            else if($scope.subcategory != null){

                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "SUBCATEGOR = '" +$scope.subcategory +"'";
                query.returnGeometry = true;
                query.orderByFields = ["COUNTY"];
                query.outFields = ["COUNTY"];

                queryTask.execute(query, function(results){
                    $scope.countiesExist = false;
                    $scope.counties = [];
                    for(var i = 1; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            if(results.features[i].attributes.County != null && results.features[i].attributes.County != " "
                            && results.features[i-1].attributes.County != null && results.features[i-1].attributes.County != " "){
                                if(results.features[i-1].attributes.County.toLowerCase() != results.features[i].attributes.County.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.County);
                                }
                            }
                        }
                        else{
                           if(results.features[i].attributes.COUNTY != null && results.features[i].attributes.COUNTY != " "
                            && results.features[i-1].attributes.COUNTY != null && results.features[i-1].attributes.COUNTY != " "){
                                if(results.features[i-1].attributes.COUNTY.toLowerCase() != results.features[i].attributes.COUNTY.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.COUNTY);
                                }
                            }
                        }
                    }
                    if($scope.counties.length > 0){
                        $scope.countiesExist = true;
                    }
                    $scope.counties = $.unique($scope.counties);
                    $scope.$apply();
                });
            }
            else if($scope.industry != null){

                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "INDUSTRY = '" +$scope.industry +"'";
                query.returnGeometry = true;
                query.orderByFields = ["COUNTY"];
                query.outFields = ["COUNTY"];

                queryTask.execute(query, function(results){
                    $scope.countiesExist = false;
                    $scope.counties = [];
                    for(var i = 1; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            if(results.features[i].attributes.County != null && results.features[i].attributes.County != " "
                            && results.features[i-1].attributes.County != null && results.features[i-1].attributes.County != " "){
                                if(results.features[i-1].attributes.County.toLowerCase() != results.features[i].attributes.County.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.County);
                                }
                            }
                        }
                        else{
                           if(results.features[i].attributes.COUNTY != null && results.features[i].attributes.COUNTY != " "
                            && results.features[i-1].attributes.COUNTY != null && results.features[i-1].attributes.COUNTY != " "){
                                if(results.features[i-1].attributes.COUNTY.toLowerCase() != results.features[i].attributes.COUNTY.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.COUNTY);
                                }
                            }
                        }
                    }
                    if($scope.counties.length > 0){
                        $scope.countiesExist = true;
                    }
                    $scope.counties = $.unique($scope.counties);
                    $scope.$apply();
                });
            }
        }

        $scope.setCounties = function(){
            if($scope.layer == null){
                $scope.counties = [];
            }
            else{
                var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                var query = new esri.tasks.Query();
                query.where = "1=1";
                query.returnGeometry = true;
                query.orderByFields = ["COUNTY"];
                query.outFields = ["COUNTY"];

                queryTask.execute(query, function(results){
                    $scope.countiesExist = false;
                    $scope.counties = [];
                    for(var i = 1; i < results.features.length; i++){
                        if($scope.layer.id == 0){
                            if(results.features[i].attributes.County != null && results.features[i].attributes.County != " "
                            && results.features[i-1].attributes.County != null && results.features[i-1].attributes.County != " "){
                                if(results.features[i-1].attributes.County.toLowerCase() != results.features[i].attributes.County.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.County);
                                }
                            }
                        }
                        else{
                           if(results.features[i].attributes.COUNTY != null && results.features[i].attributes.COUNTY != " "
                            && results.features[i-1].attributes.COUNTY != null && results.features[i-1].attributes.COUNTY != " "){
                                if(results.features[i-1].attributes.COUNTY.toLowerCase() != results.features[i].attributes.COUNTY.toLowerCase()){
                                    $scope.counties.push(results.features[i].attributes.COUNTY);
                                }
                            }
                        }
                    }
                    if($scope.counties.length > 0){
                        $scope.countiesExist = true;
                    }
                    $scope.counties = $.unique($scope.counties);
                    $scope.$apply();
                });
            }
        }

        function getAllKentuckyCounties(){
            var countiesURL = "http://maps2.bgadd.org/arcgis/rest/services/KADIS/KY_Base/MapServer/0";
            var queryTask = esri.tasks.QueryTask(countiesURL);
            var query = new esri.tasks.Query();
            query.where = "1=1";
            query.outSpatialReference = $scope.map.spatialReference;
            query.returnGeometry = true;
            query.orderByFields = ["NAME"];
            query.outFields = ["*"];

            queryTask.execute(query, function(results){
                $scope.allKentuckyCounties = [];
                for(var i = 0; i < results.features.length; i++){
                    $scope.allKentuckyCounties.push(results.features[i]);
                }
                $scope.$apply();
            });
        }

        $scope.zoomToCounty = function(){
            if($scope.kyCounty != null){
                if($scope.countyLayer){
                    $scope.map.removeLayer($scope.countyLayer);
                }
                var extent = $scope.kyCounty.geometry.getExtent();
                $scope.map.setExtent(extent, function(){ console.log('here'); });

                var symbol = new esri.symbol.SimpleLineSymbol();
                $scope.countyLayer = new esri.layers.GraphicsLayer();
                $scope.map.addLayer($scope.countyLayer);
                $scope.countyLayer.add($scope.kyCounty.setSymbol(symbol));
            }
            else {
                if($scope.countyLayer){
                    $scope.map.removeLayer($scope.countyLayer);
                    $scope.map.removeLayer($scope.countyLayer);
                    var center = [-86, 37.6];
                    var zoomLevel = 8;
                    $scope.map.centerAndZoom(center, zoomLevel);
                }
            }
            $scope.$apply();
        }

        $scope.viewInventory = function(){
            var divWithID = document.getElementsByClassName('farmersMarketTitle');
            var divWithID = angular.element(divWithID);
            var kadis_id = divWithID[0].id;
            console.log(kadis_id);

            for(i in $scope.farmersMarkets){
                if($scope.farmersMarkets[i].kadis_id == kadis_id){
                    $scope.selectedMarket = $scope.farmersMarkets[i];
                    break;
                }
            }

            $.ajax({
                type: "POST", 
                url: "index/get_farmers_market_inventory",
                data:{
                    "kadis_id": kadis_id
                },
                success: function(response) {
                    if(response != 'none'){
                        $scope.currentInventory = JSON.parse(response);
                        console.log($scope.currentInventory);
                    }
                    else{
                        $scope.currentInventory = "";
                    }
                    getLastUpdatedOn(kadis_id);
                }
            });
            
        }

        function getLastUpdatedOn(kadis_id){
            $.ajax({
                type: "POST", 
                url: "index/get_last_updated_on",
                data:{
                    "kadis_id": kadis_id
                },
                success: function(response) {
                    $scope.lastUpdatedOn = response;
                    $scope.$apply();
                }
            });
        }

        $scope.searchLayer = function(){

            //hide everything first
            if($scope.featureLayer){
                $scope.map.removeLayer($scope.featureLayer);
            }
            if($scope.countyLayer){
                $scope.map.removeLayer($scope.countyLayer);

            }

            $scope.hideAll();
            $scope.hideAllThematic();
            $scope.map.graphics.clear();

            //add the user's current location back
            $scope.addCurrentLocationToMap();

            //just toggle the layer on
            if($scope.searchOption == "showAll" && $scope.subcategory == null && $scope.industry == null && $scope.item == null){
                $scope.hideAll();
                $scope.toggleVisiblityOfLayer($scope.layer.id);
            }

            //specific conditions given
            else{


                //the layer, but filter via subcategory and/or industry
                if($scope.searchOption == "showAll" && $scope.subcategory != null || $scope.industry != null){
                    var query = new esri.tasks.Query();
                    var queryTask = esri.tasks.QueryTask($scope.serviceURL+'/'+$scope.layer.id);
                    query.outFields = ["*"];

                    if($scope.subcategory != null && $scope.industry != null){
                        query.where = "SUBCATEGOR = '" +$scope.subcategory +"' AND INDUSTRY = '" +$scope.industry +"'";
                    }
                    else if($scope.subcategory != null){
                        query.where = "SUBCATEGOR = '" +$scope.subcategory +"'";
                    }
                    else if($scope.industry != null){
                        query.where = "INDUSTRY = '" +$scope.industry +"'";
                    }

                    query.returnGeometry = true;

                    queryTask.execute(query, function(results){
                        console.log(results);

                        var features = results.features;

                        var layerSymbol = new esri.symbol.PictureMarkerSymbol($scope.layer.icon+'.png', 51, 51);
                        // var inBuffer = [];
                  
                        for (var i = 0; i < features.length; i++) {

                            features[i].setSymbol(layerSymbol);
                            $scope.map.graphics.add(features[i]);
                        }

                        if(features.length == 1){

                            var point = new esri.geometry.Point(features[0].geometry);
                            console.log(point);
                            var gsvc = new esri.tasks.GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
                            var outSR = new esri.SpatialReference(4326);

                            gsvc.project([ point ], outSR, function(projectedPoints){
                                console.log(projectedPoints);
                                var points = projectedPoints[0];
                                var zoomLevel = 12;
                                $scope.map.centerAndZoom(points, zoomLevel);
                            });

                        }
                        if(features.length > 1){
                            var minX = features[0].geometry.x;
                            var maxX = features[0].geometry.x;
                            var minY = features[0].geometry.y;
                            var maxY = features[0].geometry.y;

                            for(var i = 1; i < features.length; i++){
                                if(features[i].geometry.x < minX){
                                    minX = features[i].geometry.x;
                                }
                                if(features[i].geometry.x > maxX){
                                    maxX = features[i].geometry.x;
                                }
                                if(features[i].geometry.y < minY){
                                    minY = features[i].geometry.y;
                                }
                                if(features[i].geometry.y > maxY){
                                    maxY = features[i].geometry.y;
                                }
                            }

                            var gsvc = new esri.tasks.GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
                            var minPoints = new esri.geometry.Point(minX, minY, new esri.SpatialReference(102763));
                            var maxPoints = new esri.geometry.Point(maxX, maxY, new esri.SpatialReference(102763));
                            var outSR = new esri.SpatialReference(4326);

                            gsvc.project([ minPoints, maxPoints ], outSR, function(projectedPoints){
                                console.log(projectedPoints);

                                var point = projectedPoints[0];
                                var point2 = projectedPoints[1];

                                //subtract .5 from min, add to max
                                var polygonJson  = {"rings":[[[point.x - 1, point.y - 1], [point2.x + 1, point2.y + 1]]], "spatialReference":{"wkid":4326}};
                                var searchPolygon = new esri.geometry.Polygon(polygonJson);

                                var extent = searchPolygon.getExtent();
                                $scope.map.setExtent(extent);

                            });
                        }

                        // identifyParams.layerIds = [];
                        // //only show results for visible layers
                        // for(i in $scope.layers){
                        //     if($scope.layers[i].visible){
                        //         identifyParams.layerIds.push($scope.layers[i].id);
                        //     }
                        // }
                        $scope.$apply();
                    });
                }

                //show all markets that carry the selected items by comparing kadis ids
                if($scope.searchOption == "showAll" && $scope.item != null){
                    var query = new esri.tasks.Query();
                    query.where = "1=1";
                    query.returnGeometry = true;

                    $scope.featureLayer.queryFeatures(query, function(results){

                        var features = results.features;
                        var inBuffer = [];
                        var marketsInBuffer = [];
                  
                        for (var i = 0; i < features.length; i++) {
                            if($scope.kadisIDsWithItems.indexOf(features[i].attributes.KADIS_ID) > -1){
                                inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                                marketsInBuffer.push(features[i]);
                            }
                        }

                        var query = new esri.tasks.Query();
                        query.objectIds = inBuffer;
                        $scope.featureLayer.selectFeatures(query);

                        if(marketsInBuffer.length == 1){

                            var point = new esri.geometry.Point(marketsInBuffer[0].geometry);
                            console.log(point);
                            var gsvc = new esri.tasks.GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
                            var outSR = new esri.SpatialReference(4326);

                            gsvc.project([ point ], outSR, function(projectedPoints){
                                console.log(projectedPoints);
                                var points = projectedPoints[0];
                                var zoomLevel = 12;
                                $scope.map.centerAndZoom(points, zoomLevel);
                            });

                        }
                        if(marketsInBuffer.length > 1){

                            console.log(marketsInBuffer);

                            var minX = marketsInBuffer[0].geometry.x;
                            var maxX = marketsInBuffer[0].geometry.x;
                            var minY = marketsInBuffer[0].geometry.y;
                            var maxY = marketsInBuffer[0].geometry.y;

                            for(var i = 1; i < marketsInBuffer.length; i++){
                                if(marketsInBuffer[i].geometry.x < minX){
                                    minX = marketsInBuffer[i].geometry.x;
                                }
                                if(marketsInBuffer[i].geometry.x > maxX){
                                    maxX = marketsInBuffer[i].geometry.x;
                                }
                                if(marketsInBuffer[i].geometry.y < minY){
                                    minY = marketsInBuffer[i].geometry.y;
                                }
                                if(marketsInBuffer[i].geometry.y > maxY){
                                    maxY = marketsInBuffer[i].geometry.y;
                                }
                            }

                            var gsvc = new esri.tasks.GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
                            var minPoints = new esri.geometry.Point(minX, minY, new esri.SpatialReference(102763));
                            var maxPoints = new esri.geometry.Point(maxX, maxY, new esri.SpatialReference(102763));
                            var outSR = new esri.SpatialReference(4326);

                            gsvc.project([ minPoints, maxPoints ], outSR, function(projectedPoints){
                                console.log(projectedPoints);

                                var point = projectedPoints[0];
                                var point2 = projectedPoints[1];

                                //subtract .2 and .3 from min, add to max
                                var polygonJson  = {"rings":[[[point.x - 1, point.y - 1], [point2.x + 1, point2.y + 1]]], "spatialReference":{"wkid":4326}};
                                var searchPolygon = new esri.geometry.Polygon(polygonJson);

                                var extent = searchPolygon.getExtent();
                                $scope.map.setExtent(extent);

                                $scope.$apply();

                            });
                        }

                    });

                }

                if($scope.searchOption == "county"){
                    var query = new esri.tasks.Query();
                    if($scope.layer.id == 0){
                        query.where = "County = '" +$scope.county+ "'";
                    }
                    else{
                        query.where = "COUNTY = '" +$scope.county+ "'";
                    }

                    query.returnGeometry = true;

                    var county = get_county_info($scope.county);
                    var extent = county.geometry.getExtent();

                    $scope.map.setExtent(extent);

                    //draw the county's shape to the map
                    var symbol = new esri.symbol.SimpleLineSymbol();
                    $scope.countyLayer = new esri.layers.GraphicsLayer();
                    $scope.map.addLayer($scope.countyLayer);
                    $scope.countyLayer.add(county.setSymbol(symbol));

                    $scope.featureLayer.queryFeatures(query, function(results){
                        var features = results.features;
                        var inBuffer = [];
                  
                        for (var i = 0; i < features.length; i++) {
                            if($scope.layer.id == 0 && $scope.kadisIDsWithItems != null){
                                if($scope.kadisIDsWithItems.indexOf(features[i].attributes.KADIS_ID) > -1){
                                    inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                                }
                            }
                            else{
                                inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                            }
                        }

                        var query = new esri.tasks.Query();
                        query.objectIds = inBuffer;
                        $scope.featureLayer.selectFeatures(query);
                        $scope.$apply();
                    });
                }

                if($scope.searchOption == "currentLocation"){

                    var center = [$scope.currentLongitude, $scope.currentLatitude];

                    if($scope.currentLocationMile == null){
                        $scope.currentLocationMile = 5;
                    }
                    var radius = $scope.currentLocationMile;

                    var circleSymb = new esri.symbol.SimpleFillSymbol(
                        esri.symbol.SimpleFillSymbol.STYLE_NULL,
                        new esri.symbol.SimpleLineSymbol(
                            esri.symbol.SimpleLineSymbol.STYLE_SHORTDASHDOTDOT,
                            new esri.Color([105, 105, 105]),
                            3
                        ), new esri.Color([255, 255, 0, 0.25])
                    );                    

                    var circle = new esri.geometry.Circle({
                        center: center,
                        geodesic: true,
                        radius: radius,
                        radiusUnit: "esriMiles"
                    });

                    var graphic = new esri.Graphic(circle, circleSymb);
                    $scope.map.graphics.add(graphic)

                    var query = new esri.tasks.Query();

                    if($scope.subcategory != null && $scope.industry != null){
                        query.where = "SUBCATEGOR = '" +$scope.subcategory +"' AND INDUSTRY = '" +$scope.industry +"'";
                    }
                    else if($scope.subcategory != null){
                        query.where = "SUBCATEGOR = '" +$scope.subcategory +"'";
                    }
                    else if($scope.industry != null){
                        query.where = "INDUSTRY = '" +$scope.industry +"'";
                    }

                    var zoomLevel = get_zoom_level(radius);
                    $scope.map.centerAndZoom(center, zoomLevel);

                    query.geometry = circle.getExtent();

                    $scope.featureLayer.queryFeatures(query, function(response){
                        var feature;
                        var features = response.features;
                        var inBuffer = [];
                  
                        for (var i = 0; i < features.length; i++) {
                            feature = features[i];
                            if(feature.geometry != null){
                                if(circle.contains(feature.geometry)){
                                    if($scope.layer.id == 0 && $scope.kadisIDsWithItems != null){
                                        if($scope.kadisIDsWithItems.indexOf(features[i].attributes.KADIS_ID) > -1){
                                            inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                                        }
                                    }
                                    else{
                                        inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                                    }
                                }
                            }
                        }

                        var query = new esri.tasks.Query();
                        query.objectIds = inBuffer;
                        $scope.featureLayer.selectFeatures(query);
                        $scope.$apply();
                    });
                }

                if($scope.searchOption == "clickRadius"){
                    var center = [$scope.currentLongitude, $scope.currentLatitude];
                    if($scope.mile == null){
                        $scope.mile = 5;
                    }
                    var radius = $scope.mile;

                    var circleSymb = new esri.symbol.SimpleFillSymbol(
                        esri.symbol.SimpleFillSymbol.STYLE_NULL,
                        new esri.symbol.SimpleLineSymbol(
                            esri.symbol.SimpleLineSymbol.STYLE_SHORTDASHDOTDOT,
                            new esri.Color([105, 105, 105]),
                            3
                        ), new esri.Color([255, 255, 0, 0.25])
                    );                    

                    $scope.map.on("click", function(evt){
                        if($scope.searchOption == "clickRadius"){
                            $scope.map.graphics.clear();
                            $scope.addCurrentLocationToMap();

                            var center = evt.mapPoint;
                            var radius = $scope.mile;

                            //check to see if we have extra padding still
                            var mapDiv = document.getElementsByClassName('mainMapContainer');
                            var mapDiv = angular.element(mapDiv);
                            
                            //if we do, get the screen points, remove 250 from x, and convert back to map coordinates
                            if(mapDiv[0].className.indexOf('padding-250') > -1){
                                var mapPoint = evt.mapPoint;     
                                var screenPoint = $scope.map.toScreen(mapPoint);
                                screenPoint.x = screenPoint.x - 250;
                                var center = $scope.map.toMap(screenPoint);
                            }

                            var circle = new esri.geometry.Circle({
                                center: center,
                                geodesic: true,
                                radius: radius,
                                radiusUnit: "esriMiles"
                            });

                            var graphic = new esri.Graphic(circle, circleSymb);
                            $scope.map.graphics.add(graphic)

                            var query = new esri.tasks.Query();
                            if($scope.subcategory != null && $scope.industry != null){
                                query.where = "SUBCATEGOR = '" +$scope.subcategory +"' AND INDUSTRY = '" +$scope.industry +"'";
                            }
                            else if($scope.subcategory != null){
                                query.where = "SUBCATEGOR = '" +$scope.subcategory +"'";
                            }
                            else if($scope.industry != null){
                                query.where = "INDUSTRY = '" +$scope.industry +"'";
                            }

                            var zoomLevel = get_zoom_level(radius);
                            $scope.map.centerAndZoom(center, zoomLevel);

                            query.geometry = circle.getExtent();

                            $scope.featureLayer.queryFeatures(query, function(response){
                                var feature;
                                var features = response.features;
                                var inBuffer = [];
                          
                                for (var i = 0; i < features.length; i++) {
                                    feature = features[i];
                                    if(feature.geometry != null){
                                        if(circle.contains(feature.geometry)){
                                            if($scope.layer.id == 0 && $scope.kadisIDsWithItems != null){
                                                if($scope.kadisIDsWithItems.indexOf(features[i].attributes.KADIS_ID) > -1){
                                                    inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                                                }
                                            }
                                            else{
                                                inBuffer.push(features[i].attributes[$scope.featureLayer.objectIdField]);
                                            }
                                        }
                                    }
                                }
                                var query = new esri.tasks.Query();
                                query.objectIds = inBuffer;
                                $scope.featureLayer.selectFeatures(query);
                                $scope.$apply();
                            });
                        }
                    });
                }
            }
        }

        function get_county_info(county){
            var countyInfo;
            for(var i = 0; i < $scope.allKentuckyCounties.length; i++){
                if($scope.allKentuckyCounties[i].attributes.NAME.toLowerCase() == county.toLowerCase()){
                    countyInfo = $scope.allKentuckyCounties[i];
                    return countyInfo;
                }
            }
        }

        function get_zoom_level(radius){
            var zoomLevel = 8;

            switch(radius){
                case 5:
                    zoomLevel = 12;
                    break;
                case 15:
                    zoomLevel = 10;
                    break;
                case 30:
                case 45:
                    zoomLevel = 9;
                    break;
                case 60:
                case 75:
                case 90:
                    zoomLevel = 8;
                    break;
                case 105:
                case 120:
                    zoomLevel = 7;
                    break;
            }

            return zoomLevel;
        }

        function get_POI_icons(){
            var legendURL = $scope.serviceURL+"/legend";
            var legendRequest = esri.request({
                url: legendURL,
                content: {
                    f: "json"
                },
                handleAs: "json"
            });
            legendRequest.then(function(data){
                for(var i = 0; i < $scope.layers.length; i++){
                    $scope.layers[i].icon = $scope.serviceURL+"/"+i+"/images/"+data.layers[i].legend[0].url;
                }
                $scope.$apply();
            }, function(error){
                console.log("Error: ", error.message);   
            });
        }

        function get_thematic_icons(){
            var legendURL = $scope.thematicURL+"/legend";
            var legendRequest = esri.request({
                url: legendURL,
                content: {
                    f: "json"
                },
                handleAs: "json"
            });
            legendRequest.then(function(data){
                // console.log(data);
                for(var i = 0; i < $scope.thematicLayers.length; i++){
                    $scope.thematicLayers[i].legend = data.layers[i].legend;
                    for(j = 0; j < $scope.thematicLayers[i].legend.length; j++){
                        $scope.thematicLayers[i].legend[j].icon = $scope.thematicURL+"/"+i+"/images/"+$scope.thematicLayers[i].legend[j].url;
                    }
                }
                $scope.$apply();
            }, function(error){
                console.log("Error: ", error.message);   
            });
        }

    }]);
    
    /*==============
    LOGIN CONTROLLER
    ==============*/
    mappingApp.controller('LoginController', ['$scope', function($scope){
        
        $scope.username = "";
        $scope.password = "";
        $scope.loginErrors = "";
        $scope.wrongLogin = false;

        $scope.email = "";
        $scope.validationMessage = "";
        $scope.forgotCreds = false;
        $scope.wrongEmail = false;
        $scope.correctEmail = false;

        $scope.verifyUser = function(){
            $.ajax({
                type: "POST", 
                url: "login/verify_user",
                data: {"username": $scope.username, "password": $scope.password},
                success: function(response) {
                    if(response == 'true'){
                        $scope.wrongLogin = false;
                        window.location.href = "farmersmarkets/dashboard";
                    }
                    else if(response == 'admin'){
                        $scope.wrongLogin = false;
                        window.location.href = "admin/dashboard";
                    }
                    else{
                        $scope.wrongLogin = true;
                        $scope.loginErrors = "Wrong username/password combination. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.forgotCredentials = function(){
            $scope.forgotCreds = true;
        }

        $scope.validateEmail = function(){
            $.ajax({
                type: "POST", 
                url: "login/forgot_email",
                data: {"email": $scope.email},
                success: function(response) {
                    if(response == 'true'){
                        $scope.wrongEmail = false;
                        $scope.correctEmail = true;
                        $scope.validationMessage = "Success! An email has been sent to "+$scope.email+" with your username and a new temporary password.";
                    }
                    else{
                        $scope.correctEmail = false;
                        $scope.wrongEmail = true;
                        $scope.validationMessage = "Sorry, we couldn't find an account associated with "+$scope.email+".";
                    }
                    $scope.$apply();
                }
            });
        }

    }]);


    /*===============
    SIGNUP CONTROLLER
    ===============*/
    mappingApp.controller('SignUpController', ['$scope', '$http', function($scope, $http){
        
        $scope.businessName = "";
        $scope.username = "";
        $scope.email = "";
        $scope.confirmEmail = "";
        $scope.password = "";
        $scope.confirmPassword = "";
        $scope.signUpErrors = "";
        $scope.errors = false;

        getEntityTypes();

        $scope.signUpNewUser = function(){
            if($scope.email != $scope.confirmEmail && $scope.password != $scope.confirmPassword){
                $scope.errors = true;
                $scope.signUpErrors = "Emails and passwords don't match.";
            }
            else if($scope.password != $scope.confirmPassword){
                $scope.errors = true;
                $scope.signUpErrors = "Passwords don't match.";
            }
            else if($scope.email != $scope.confirmEmail){
                $scope.errors = true;
                $scope.signUpErrors = "Emails don't match.";
            }
            else{
                $scope.errors = false;
                // console.log("Signed up!");

                $.ajax({
                    type: "POST", 
                    url: "login/sign_up",
                    data:{
                        "name": $scope.businessName,
                        "entity_type":  $scope.entityType,
                        "username": $scope.username,
                        "email": $scope.email,
                        "password": $scope.password
                    },
                    success: function(response) {
                        if(response == 'success'){
                            window.location.href = "login/basic_info";
                        }
                        else if(response == 'Username in use'){
                            $scope.errors = true;
                            $scope.signUpErrors = "That username is already in use.";
                        }
                        else{
                            $scope.errors = true;
                            $scope.signUpErrors = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }

        }

        function getEntityTypes(){
            $http.get('login/get_entity_types').success(function(data) { 
                $scope.entityTypes = data;
                $scope.entityType = $scope.entityTypes[0];
            });
        }

    }]);
    
    /*===================
    BASIC INFO CONTROLLER
    ===================*/

    mappingApp.controller('BasicInfoController', ['$scope', '$http', function($scope, $http){

        $scope.address = "";
        $scope.city = "";
        $scope.zipcode = "";
        $scope.hours = "";
        $scope.website = "";
        $scope.contactName = "";
        $scope.contactPhone = "";
        $scope.basicInfoErrors = "";
        $scope.basicErrors = false;

        $scope.email = "";
        $scope.confirmEmail = "";
        $scope.password = "";
        $scope.confirmPassword = "";

        getBaseURL();
        getStates();

        $scope.completeSignUpProcess = function(){
            if($scope.email != $scope.confirmEmail && $scope.password != $scope.confirmPassword){
                $scope.basicErrors = true;
                $scope.basicInfoErrors = "Emails and passwords don't match.";
            }
            else if($scope.password != $scope.confirmPassword){
                $scope.basicErrors = true;
                $scope.basicInfoErrors = "Passwords don't match.";
            }
            else if($scope.email != $scope.confirmEmail){
                $scope.basicErrors = true;
                $scope.basicInfoErrors = "Emails don't match.";
            }
            else{
                $.ajax({
                    type: "POST", 
                    url: "save_basic_info",
                    data:{
                        "address": $scope.address,
                        "city":  $scope.city,
                        "state": $scope.state.abbreviation,
                        "zipcode": $scope.zipcode,
                        "hours": $scope.hours,
                        "website": $scope.website,
                        "contact_name": $scope.contactName,
                        "contact_phone": $scope.contactPhone,
                        "password": $scope.password,
                        "email": $scope.email
                    },
                    success: function(response) {
                        if(response == 'success'){
                            $scope.basicErrors = false;

                            window.location.href = $scope.baseURL+"farmersmarkets/dashboard";
                        }
                        else{
                            $scope.basicErrors = true;
                            $scope.basicInfoErrors = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }
        }

        function getBaseURL(){
            $http.get('get_base_url').success(function(data){ 
                $scope.baseURL = data;
                console.log($scope.baseURL);
            });
        }

        function getStates(){
            $scope.states = [
                { name: 'ALABAMA', abbreviation: 'AL'},
                { name: 'ALASKA', abbreviation: 'AK'},
                { name: 'ARIZONA', abbreviation: 'AZ'},
                { name: 'ARKANSAS', abbreviation: 'AR'},
                { name: 'CALIFORNIA', abbreviation: 'CA'},
                { name: 'COLORADO', abbreviation: 'CO'},
                { name: 'CONNECTICUT', abbreviation: 'CT'},
                { name: 'DELAWARE', abbreviation: 'DE'},
                { name: 'DISTRICT OF COLUMBIA', abbreviation: 'DC'},
                { name: 'FLORIDA', abbreviation: 'FL'},
                { name: 'GEORGIA', abbreviation: 'GA'},
                { name: 'HAWAII', abbreviation: 'HI'},
                { name: 'IDAHO', abbreviation: 'ID'},
                { name: 'ILLINOIS', abbreviation: 'IL'},
                { name: 'INDIANA', abbreviation: 'IN'},
                { name: 'IOWA', abbreviation: 'IA'},
                { name: 'KANSAS', abbreviation: 'KS'},
                { name: 'KENTUCKY', abbreviation: 'KY'},
                { name: 'LOUISIANA', abbreviation: 'LA'},
                { name: 'MAINE', abbreviation: 'ME'},
                { name: 'MARYLAND', abbreviation: 'MD'},
                { name: 'MASSACHUSETTS', abbreviation: 'MA'},
                { name: 'MICHIGAN', abbreviation: 'MI'},
                { name: 'MINNESOTA', abbreviation: 'MN'},
                { name: 'MISSISSIPPI', abbreviation: 'MS'},
                { name: 'MISSOURI', abbreviation: 'MO'},
                { name: 'MONTANA', abbreviation: 'MT'},
                { name: 'NEBRASKA', abbreviation: 'NE'},
                { name: 'NEVADA', abbreviation: 'NV'},
                { name: 'NEW HAMPSHIRE', abbreviation: 'NH'},
                { name: 'NEW JERSEY', abbreviation: 'NJ'},
                { name: 'NEW MEXICO', abbreviation: 'NM'},
                { name: 'NEW YORK', abbreviation: 'NY'},
                { name: 'NORTH CAROLINA', abbreviation: 'NC'},
                { name: 'NORTH DAKOTA', abbreviation: 'ND'},
                { name: 'OHIO', abbreviation: 'OH'},
                { name: 'OKLAHOMA', abbreviation: 'OK'},
                { name: 'OREGON', abbreviation: 'OR'},
                { name: 'PENNSYLVANIA', abbreviation: 'PA'},
                { name: 'RHODE ISLAND', abbreviation: 'RI'},
                { name: 'SOUTH CAROLINA', abbreviation: 'SC'},
                { name: 'SOUTH DAKOTA', abbreviation: 'SD'},
                { name: 'TENNESSEE', abbreviation: 'TN'},
                { name: 'TEXAS', abbreviation: 'TX'},
                { name: 'UTAH', abbreviation: 'UT'},
                { name: 'VERMONT', abbreviation: 'VT'},
                { name: 'VIRGINIA', abbreviation: 'VA'},
                { name: 'WASHINGTON', abbreviation: 'WA'},
                { name: 'WEST VIRGINIA', abbreviation: 'WV'},
                { name: 'WISCONSIN', abbreviation: 'WI'},
                { name: 'WYOMING', abbreviation: 'WY' }
            ];

            $scope.state = $scope.states[17];
        }
    }]);


    /*==================
    DASHBOARD CONTROLLER
    ==================*/

    mappingApp.controller('DashboardController', ['$scope', '$http', '$sce', function($scope, $http, $sce){

        getUser();

        $scope.basicErrors = false;
        $scope.basicInfoErrors = "";
        $scope.edittingBasic = false;
        $scope.edittingAccount = false;

        $scope.resettingPassword = false;
        $scope.password = "";
        $scope.confirmPassword = "";
        
        $scope.errors = false;
        $scope.success = false;

        $scope.successMessage = "";
        $scope.errorMessage = "";

        $scope.toggleEdit = function(){
            getStates();
            if($scope.edittingBasic){
                $('.editInfoModal').modal('hide');
                $scope.edittingBasic = false;
            } else {
                $('.editInfoModal').modal('show');
                $scope.edittingBasic = true;
            }
        }
        $('.editInfoModal').on('hidden.bs.modal', function (e) {
            $scope.edittingBasic = false;
            $scope.$apply();
        })

        $scope.togglePasswordReset = function(){
            if($scope.resettingPassword){
                $('.resetPasswordModal').modal('hide');
                $scope.resettingPassword = false;
                $scope.password = "";
                $scope.confirmPassword = "";
            } else {
                $('.resetPasswordModal').modal('show');
                $scope.resettingPassword = true;
            }
        }
        $('.resetPasswordModal').on('hidden.bs.modal', function (e) {
            $scope.resettingPassword = false;
            $scope.password = "";
            $scope.confirmPassword = "";
            $scope.$apply();
        })

        $scope.resetPassword = function(){
            if($scope.password != $scope.confirmPassword){
                $scope.errors = true;
                $scope.errorMessage = "Passwords do not match.";

                $scope.$apply();
            }
            else{
                $.ajax({
                    type: "POST", 
                    url: "dashboard/reset_password",
                    data:{
                        "password": $scope.password
                    },
                    success: function(response) {
                        console.log(response);
                        if(response == 'saved'){
                            $scope.errors = false;
                            $scope.errorMessage = "";

                            $scope.success = true;
                            $scope.successMessage = "Password changed!";
                            
                            $scope.togglePasswordReset();
                            getUser();
                        }
                        else{
                            $scope.errors = true;
                            $scope.errorMessage = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }
        }

        $scope.saveNewInfo = function(){

            $.ajax({
                type: "POST", 
                url: "dashboard/edit_basic_info",
                data:{
                    "name": $scope.name,
                    "address": $scope.address,
                    "city":  $scope.city,
                    "state": $scope.state.abbreviation,
                    "zipcode": $scope.zipcode,
                    "hours": $scope.hours,
                    "website": $scope.website,
                    "contact_name": $scope.contactName,
                    "contact_phone": $scope.contactPhone
                },
                success: function(response) {
                    if(response == 'success'){
                        $scope.basicErrors = false;
                        $scope.basicInfoErrors = "";
                        $scope.toggleEdit();
                        getEntity();
                    }
                    else{
                        $scope.basicErrors = true;
                        $scope.basicInfoErrors = "There was an error with our server. Please try again.";
                    }
                    // $scope.$apply();
                }
            });
        }

        $scope.toggleAccountSettings = function(){
            if($scope.edittingAccount){
                $('.editAccountModal').modal('hide');
                $scope.edittingAccount = false;
            } else {
                $('.editAccountModal').modal('show');
                $scope.edittingAccount = true;
            }
        }
        $('.editAccountModal').on('hidden.bs.modal', function (e) {
            $scope.edittingAccount = false;
            $scope.$apply();
        })

        $scope.saveAccountInfo = function() {
            $scope.saveUsername();
            // $scope.$apply();
        }

        $scope.saveUsername = function(){
            $.ajax({
                type: "POST", 
                url: "dashboard/save_username",
                data:{
                    "username": $scope.username
                },
                success: function(response) {
                    if(response == 'saved'){
                        $scope.errors = false;
                        $scope.errorMessage = "";

                        $scope.success = true;
                        $scope.successMessage = "Username changed!";
                        
                        $scope.saveEmail();
                    }
                    else{
                        $scope.errors = true;
                        $scope.errorMessage = "That username is already in use.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.saveEmail = function(){
            $.ajax({
                type: "POST", 
                url: "dashboard/save_email_address",
                data:{
                    "email": $scope.email
                },
                success: function(response) {
                    if(response == 'saved'){
                        $scope.errors = false;
                        $scope.errorMessage = "";

                        $scope.success = true;
                        $scope.successMessage = "Email changed!";
                        
                        $scope.edittingAccount = false;
                        $('.editAccountModal').modal('hide');
                        getUser();
                    }
                    else{
                        $scope.errors = true;
                        $scope.errorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.addToInventory = function(produce){
            
            var name = produce.name;
            var type = produce.type;
           
            $.ajax({
                type: "POST", 
                url: "dashboard/add_to_inventory",
                data:{
                    "name": name,
                    "type": type
                },
                success: function(response) {
                    if(response == 'saved'){
                        getInventory();
                    }
                    else{
                        alert("There was an error with our server. Please try again.");
                    }
                }
            });
        }

        $scope.removeFromInventory = function(produce){
      
            var entity_inventory_id = produce.id;

            $.ajax({
                type: "POST", 
                url: "dashboard/remove_from_inventory",
                data:{
                    "entity_inventory_id": entity_inventory_id
                },
                success: function(response) {
                    if(response == 'removed'){
                        getInventory();
                    }
                    else{
                        alert("There was an error with our server. Please try again.");
                    }
                }
            });

        }

        $scope.showDetails = function(id){
            for(i in $scope.currentInventory){
                if($scope.currentInventory[i].id == id){
                    if($scope.currentInventory[i].detailsVisibile){
                        $scope.currentInventory[i].detailsVisibile = false;
                    } 
                    else{
                        $scope.currentInventory[i].detailsVisibile = true;
                    }
                }
            }
        }

        $scope.saveDetails = function(produce){

            $.ajax({
                type: "POST", 
                url: "dashboard/save_details",
                data:{
                    "entity_inventory_id": produce.id,
                    "details": produce.details
                },
                success: function(response) {
                    if(response == 'saved'){

                        for(i in $scope.currentInventory){
                            if($scope.currentInventory[i].id == produce.id){
                                $scope.currentInventory[i].detailsVisibile = false;
                            }
                        }

                        getInventory();
                    }
                    else{
                        alert("There was an error with our server. Please try again.");
                    }
                }
            });
        }

        function getUser(){
            $http.get('dashboard/get_user_info').success(function(data){ 
                $scope.user = data;
                console.log($scope.user);
                $scope.username = $scope.user.username;
                $scope.email = $scope.user.email;
                getEntity();
            });
        }

        function getEntity(){
            $http.get('dashboard/get_entity_info').success(function(data){ 
                $scope.entity = data;

                $scope.name = $scope.entity.name;
                $scope.address = $scope.entity.address;
                $scope.city = $scope.entity.city;
                $scope.zipcode = $scope.entity.zipcode;
                $scope.hours = $scope.entity.hours;
                $scope.website = $scope.entity.website;
                $scope.contactName = $scope.entity.contact_name;
                $scope.contactPhone = $scope.entity.contact_phone;

                $scope.entity.hours = $sce.trustAsHtml($scope.entity.hours);
                $scope.hours = $scope.hours.replace(/<\/?[^>]+>/gi, '');

                // console.log($scope.entity);

                getInventory();
            });
        }

        function getInventory(){
            $http.get('dashboard/get_current_inventory').success(function(data){ 
                $scope.currentInventory = data;

                console.log($scope.currentInventory);

                for(i in $scope.currentInventory){
                    $scope.currentInventory[i].detailsVisibile = false;
                }

                getAllProduce();
            });
        }

        //get all produce and produce types not in inventory
        function getAllProduce(){
            $http.get('dashboard/get_all_produce').success(function(data){ 
                $scope.allProduce = data;

                filterAvailableProduce();
            });
        }

        function filterAvailableProduce(){

            for(i in $scope.currentInventory){
                var item = $scope.currentInventory[i];
                for(j in $scope.allProduce){
                    if($scope.allProduce[j].name == item.name && $scope.allProduce[j].type == item.type){
                        $scope.allProduce.splice(j, 1);
                    }    
                }
            }

            getLastUpdatedOn();
        }

        function getLastUpdatedOn(){
            $http.get('dashboard/get_last_updated_on').success(function(data){ 
                $scope.lastUpdatedOn = data;
            });
        }

        function getStates(){
            $scope.states = [
                { name: 'ALABAMA', abbreviation: 'AL'},
                { name: 'ALASKA', abbreviation: 'AK'},
                { name: 'ARIZONA', abbreviation: 'AZ'},
                { name: 'ARKANSAS', abbreviation: 'AR'},
                { name: 'CALIFORNIA', abbreviation: 'CA'},
                { name: 'COLORADO', abbreviation: 'CO'},
                { name: 'CONNECTICUT', abbreviation: 'CT'},
                { name: 'DELAWARE', abbreviation: 'DE'},
                { name: 'DISTRICT OF COLUMBIA', abbreviation: 'DC'},
                { name: 'FLORIDA', abbreviation: 'FL'},
                { name: 'GEORGIA', abbreviation: 'GA'},
                { name: 'HAWAII', abbreviation: 'HI'},
                { name: 'IDAHO', abbreviation: 'ID'},
                { name: 'ILLINOIS', abbreviation: 'IL'},
                { name: 'INDIANA', abbreviation: 'IN'},
                { name: 'IOWA', abbreviation: 'IA'},
                { name: 'KANSAS', abbreviation: 'KS'},
                { name: 'KENTUCKY', abbreviation: 'KY'},
                { name: 'LOUISIANA', abbreviation: 'LA'},
                { name: 'MAINE', abbreviation: 'ME'},
                { name: 'MARYLAND', abbreviation: 'MD'},
                { name: 'MASSACHUSETTS', abbreviation: 'MA'},
                { name: 'MICHIGAN', abbreviation: 'MI'},
                { name: 'MINNESOTA', abbreviation: 'MN'},
                { name: 'MISSISSIPPI', abbreviation: 'MS'},
                { name: 'MISSOURI', abbreviation: 'MO'},
                { name: 'MONTANA', abbreviation: 'MT'},
                { name: 'NEBRASKA', abbreviation: 'NE'},
                { name: 'NEVADA', abbreviation: 'NV'},
                { name: 'NEW HAMPSHIRE', abbreviation: 'NH'},
                { name: 'NEW JERSEY', abbreviation: 'NJ'},
                { name: 'NEW MEXICO', abbreviation: 'NM'},
                { name: 'NEW YORK', abbreviation: 'NY'},
                { name: 'NORTH CAROLINA', abbreviation: 'NC'},
                { name: 'NORTH DAKOTA', abbreviation: 'ND'},
                { name: 'OHIO', abbreviation: 'OH'},
                { name: 'OKLAHOMA', abbreviation: 'OK'},
                { name: 'OREGON', abbreviation: 'OR'},
                { name: 'PENNSYLVANIA', abbreviation: 'PA'},
                { name: 'RHODE ISLAND', abbreviation: 'RI'},
                { name: 'SOUTH CAROLINA', abbreviation: 'SC'},
                { name: 'SOUTH DAKOTA', abbreviation: 'SD'},
                { name: 'TENNESSEE', abbreviation: 'TN'},
                { name: 'TEXAS', abbreviation: 'TX'},
                { name: 'UTAH', abbreviation: 'UT'},
                { name: 'VERMONT', abbreviation: 'VT'},
                { name: 'VIRGINIA', abbreviation: 'VA'},
                { name: 'WASHINGTON', abbreviation: 'WA'},
                { name: 'WEST VIRGINIA', abbreviation: 'WV'},
                { name: 'WISCONSIN', abbreviation: 'WI'},
                { name: 'WYOMING', abbreviation: 'WY' }
            ];

            for(i in $scope.states){
                if($scope.states[i].abbreviation == $scope.entity.state){
                    $scope.state = $scope.states[i];
                }
            }
        }

    }]);
    
    /*========================
    ADMIN DASHBOARD CONTROLLER
    ========================*/

    mappingApp.controller('AdminDashboardController', ['$scope', '$http', '$sce', function($scope, $http, $sce){

        getUser();

        $scope.addingNewUser = false;
        $scope.addingNewItem = false;
        $scope.addingNewType = false;

        $scope.removingItem = false;
        $scope.itemToRemove = "";

        $scope.removeItemErrors = false;
        $scope.removeItemMessage = "";

        $scope.newUserErrors = false;
        $scope.newUserErrorMessage = "";

        $scope.newItem = "";
        $scope.newItemType = "";
        $scope.typesExist = false;

        $scope.newItemTypeErrors = false;
        $scope.newItemTypeErrorMessage = "";

        $scope.newUserEntityName = "";
        $scope.newUserKadisID = "";

        $scope.resettingPassword = false;
        $scope.password = "";
        $scope.confirmPassword = "";

        $scope.toggleAddUserModal = function(){
            if($scope.addingNewUser){
                $('.addUserModal').modal('hide');
                $scope.addingNewUser = false;
            } else {
                $('.addUserModal').modal('show');
                $scope.addingNewUser = true;
            }
        }
        $('.addUserModal').on('hidden.bs.modal', function (e) {
            $scope.addingNewUser = false;
            $scope.$apply();
        })

        $scope.toggleAddNewItemModal = function(){
            if($scope.addingNewItem){
                $('.addItemModal').modal('hide');
                $scope.addingNewItem = false;
            } else {
                $('.addItemModal').modal('show');
                $scope.addingNewItem = true;
            }
        }
        $('.addItemModal').on('hidden.bs.modal', function (e) {
            $scope.addingNewItem = false;
            $scope.$apply();
        })

        $scope.toggleAccountSettings = function(){
            if($scope.edittingAccount){
                $('.editAccountModal').modal('hide');
                $scope.edittingAccount = false;
            } else {
                $('.editAccountModal').modal('show');
                $scope.edittingAccount = true;
            }
        }
        $('.editAccountModal').on('hidden.bs.modal', function (e) {
            $scope.edittingAccount = false;
            $scope.$apply();
        })

        $scope.saveAccountInfo = function() {
            $scope.saveUsername();
            // $scope.$apply();
        }

        $scope.saveUsername = function(){
            $.ajax({
                type: "POST", 
                url: "dashboard/save_username",
                data:{
                    "username": $scope.username
                },
                success: function(response) {
                    if(response == 'saved'){
                        $scope.errors = false;
                        $scope.errorMessage = "";

                        $scope.success = true;
                        $scope.successMessage = "Username changed!";
                        
                        $scope.saveEmail();
                    }
                    else{
                        $scope.errors = true;
                        $scope.errorMessage = "That username is already in use.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.saveEmail = function(){
            $.ajax({
                type: "POST", 
                url: "dashboard/save_email_address",
                data:{
                    "email": $scope.email
                },
                success: function(response) {
                    if(response == 'saved'){
                        $scope.errors = false;
                        $scope.errorMessage = "";

                        $scope.success = true;
                        $scope.successMessage = "Email changed!";
                        
                        $scope.edittingAccount = false;
                        $('.editAccountModal').modal('hide');
                        getUser();
                    }
                    else{
                        $scope.errors = true;
                        $scope.errorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.showItemInfo = function(produce){

            $scope.typesExist = false;
            $scope.currentItem = produce;

            $.ajax({
                type: "POST", 
                url: "dashboard/get_types",
                data:{
                    "produce_id": produce.id
                },
                success: function(response) {
                    if(response.length){
                        $scope.typesExist = true;
                        $scope.currentItemTypes = JSON.parse(response);
                    } else {
                        $scope.typesExist = false;
                        $scope.currentItemTypes = "";
                    }
                    $('.itemInfoModal').modal('show');
                    $scope.$apply();
                }
            });
        }

        $scope.saveNewItemType = function(produceid){
            console.log(produceid);
            console.log($scope.newItemType);

            $.ajax({
                type: "POST", 
                url: "dashboard/add_new_type",
                data:{
                    "name": $scope.newItemType,
                    "produce_id": produceid
                },
                success: function(response) {
                    if(response == 'success'){
                        $scope.newItemTypeErrors = false;
                        $scope.newItemTypeErrorMessage = "";
                        $scope.newItemType = "";

                        for(i in $scope.allProduce){
                            if($scope.allProduce[i].id == produceid){
                                var produce = $scope.allProduce[i];
                            }
                        }

                        $scope.showItemInfo(produce);
                    } else if(response == 'exists'){
                        $scope.newItemTypeErrors = true;
                        $scope.newItemTypeErrorMessage = "This item already has this type.";
                    } else {
                        $scope.newItemTypeErrors = true;
                        $scope.newItemTypeErrorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.hideItemInfoModal = function(){
            $('.itemInfoModal').modal('hide');
            $scope.newItemTypeErrors = false;
            $scope.newItemTypeErrorMessage = "";
            $scope.newItemType = "";
            // $scope.$apply();
        }
        $('.itemInfoModal').on('hidden.bs.modal', function (e) {
            $scope.newItemTypeErrors = false;
            $scope.newItemTypeErrorMessage = "";
            $scope.newItemType = "";
            $scope.$apply();
        })

        $scope.saveNewUser = function(){

            $.ajax({
                type: "POST", 
                url: "dashboard/add_new_user",
                data:{
                    "name": $scope.newUserEntityName,
                    "kadis_id": $scope.newUserKadisID
                },
                success: function(response) {
                    if(response == 'success'){
                        $('.addUserModal').modal('hide');
                        $scope.addingNewUser = false;
                        $scope.newUserErrors = false;
                        $scope.newUserErrorMessage = "";
                        
                        $scope.newUserKadisID = "";
                        $scope.newUserEntityName = "";

                    } else if(response == 'not unique'){
                        $scope.newUserErrors = true;
                        $scope.newUserErrorMessage = "This KADIS ID is already in use.";
                    } else {
                        $scope.newUserErrors = true;
                        $scope.newUserErrorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.saveNewItem = function(){

            $.ajax({
                type: "POST", 
                url: "dashboard/add_new_item",
                data:{
                    "name": $scope.newItem
                },
                success: function(response) {
                    if(response == 'success'){
                        
                        $('.addItemModal').modal('hide');
                        
                        $scope.addingNewItem = false;
                        $scope.newItemErrors = false;
                        $scope.newItemErrorMessage = "";
                        $scope.newItem = "";

                        getAllProduce();

                    } else if(response == 'exists'){
                        $scope.newItemErrors = true;
                        $scope.newItemErrorMessage = "This item already exists.";
                    } else {
                        $scope.newItemErrors = true;
                        $scope.newItemErrorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.toggleRemoveModal = function(produce){

            if($scope.removingItem){

                $scope.itemToRemove = "";
                $scope.numberOfMarketsWithItem = "";
                $scope.numberOfItemTypes = "";

                $('.removeItemModal').modal('hide');
                $scope.removingItem = false;
            } else {

                $scope.itemToRemove = produce;
                getNumberOfMarketsWithThisItem(produce);

                $('.removeItemModal').modal('show');
                $scope.removingItem = true;

            }
        }
        $('.removeItemModal').on('hidden.bs.modal', function (e) {
            $scope.itemToRemove = "";
            $scope.numberOfMarketsWithItem = "";
            $scope.numberOfItemTypes = "";
            $scope.removingItem = false;
            $scope.$apply();
        })

        $scope.removeItem = function(itemToRemove){

            $.ajax({
                type: "POST", 
                url: "dashboard/delete_item",
                data:{
                    "produce_id": itemToRemove.id
                },
                success: function(response) {
                    if(response == 'success'){
                        
                        $('.removeItemModal').modal('hide');
                        
                        $scope.removingItem = false;
                        $scope.removeItemMessage = $scope.itemToRemove.name + " successfully removed!";
                        $scope.removeItemErrors = false;

                        alert($scope.removeItemMessage);
                        
                        getAllProduce();

                    } else {
                        $scope.removeItemErrors = true;
                        $scope.removingItem = true;
                        $scope.removeItemMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });

        }

        $scope.togglePasswordReset = function(){
            if($scope.resettingPassword){
                $('.resetPasswordModal').modal('hide');
                $scope.resettingPassword = false;
                $scope.password = "";
                $scope.confirmPassword = "";
            } else {
                $('.resetPasswordModal').modal('show');
                $scope.resettingPassword = true;
            }
        }
        $('.resetPasswordModal').on('hidden.bs.modal', function (e) {
            $scope.resettingPassword = false;
            $scope.password = "";
            $scope.confirmPassword = "";
            $scope.$apply();
        })

        $scope.resetPassword = function(){
            if($scope.password != $scope.confirmPassword){
                $scope.errors = true;
                $scope.errorMessage = "Passwords do not match.";

                $scope.$apply();
            }
            else{
                $.ajax({
                    type: "POST", 
                    url: "dashboard/reset_password",
                    data:{
                        "password": $scope.password
                    },
                    success: function(response) {
                        console.log(response);
                        if(response == 'saved'){
                            $scope.errors = false;
                            $scope.errorMessage = "";

                            $scope.success = true;
                            $scope.successMessage = "Password changed!";
                            
                            $scope.togglePasswordReset();
                            getUser();
                        }
                        else{
                            $scope.errors = true;
                            $scope.errorMessage = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }
        }

        function getUser(){
            $http.get('dashboard/get_user_info').success(function(data){ 
                $scope.user = data;
                console.log($scope.user);
                $scope.username = $scope.user.username;
                $scope.email = $scope.user.email;

                getAllProduce();
            });
        }

        //get all produce and produce types not in inventory
        function getAllProduce(){
            $http.get('dashboard/get_all_produce').success(function(data){ 
                $scope.allProduce = data;
            });
        }

        function getNumberOfMarketsWithThisItem(produce){
            $http.get('dashboard/get_number_of_markets_with_item/'+produce.id).success(function(data){ 
                $scope.numberOfMarketsWithItem = data;

                 getNumberOfItemTypes(produce);
            });
        }

        function getNumberOfItemTypes(produce){
            $http.get('dashboard/get_number_of_item_types/'+produce.id).success(function(data){ 
                $scope.numberOfItemTypes = data;
            });
        }

    }]);


    /*========================
    ADMIN USER CONTROLLER
    ========================*/

    mappingApp.controller('AdminUserController', ['$scope', '$http', '$sce', function($scope, $http, $sce){

        getUser();

        $scope.addingNewUser = false;
        $scope.newUserErrors = false;
        $scope.newUserErrorMessage = "";

        $scope.newUserEntityName = "";
        $scope.newUserKadisID = "";

        $scope.removingUser = false;
        $scope.removeUserMessage = "";
        $scope.removeUserErrors = false;

        $scope.resettingUserPassword = false;
        $scope.resetUserPasswordErrors = "";

        $scope.userPassword = "";
        $scope.userConfirmPassword = "";

        $scope.resettingPassword = false;
        $scope.password = "";
        $scope.confirmPassword = "";


        $scope.toggleAddUserModal = function(){
            if($scope.addingNewUser){
                $('.addUserModal').modal('hide');
                $scope.addingNewUser = false;
            } else {
                $('.addUserModal').modal('show');
                $scope.addingNewUser = true;
            }
        }
        $('.addUserModal').on('hidden.bs.modal', function (e) {
            $scope.addingNewUser = false;
            $scope.$apply();
        })


        $scope.toggleAccountSettings = function(){
            if($scope.edittingAccount){
                $('.editAccountModal').modal('hide');
                $scope.edittingAccount = false;
            } else {
                $('.editAccountModal').modal('show');
                $scope.edittingAccount = true;
            }
        }
        $('.editAccountModal').on('hidden.bs.modal', function (e) {
            $scope.edittingAccount = false;
            $scope.$apply();
        })

        $scope.saveAccountInfo = function() {
            $scope.saveUsername();
            // $scope.$apply();
        }

        $scope.saveUsername = function(){
            $.ajax({
                type: "POST", 
                url: "users/save_username",
                data:{
                    "username": $scope.username
                },
                success: function(response) {
                    if(response == 'saved'){
                        $scope.errors = false;
                        $scope.errorMessage = "";

                        $scope.success = true;
                        $scope.successMessage = "Username changed!";
                        
                        $scope.saveEmail();
                    }
                    else{
                        $scope.errors = true;
                        $scope.errorMessage = "That username is already in use.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.saveEmail = function(){
            $.ajax({
                type: "POST", 
                url: "users/save_email_address",
                data:{
                    "email": $scope.email
                },
                success: function(response) {
                    if(response == 'saved'){
                        $scope.errors = false;
                        $scope.errorMessage = "";

                        $scope.success = true;
                        $scope.successMessage = "Email changed!";
                        
                        $scope.edittingAccount = false;
                        $('.editAccountModal').modal('hide');
                        getUser();
                    }
                    else{
                        $scope.errors = true;
                        $scope.errorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }

        $scope.saveNewUser = function(){

            $.ajax({
                type: "POST", 
                url: "users/add_new_user",
                data:{
                    "name": $scope.newUserEntityName,
                    "kadis_id": $scope.newUserKadisID
                },
                success: function(response) {
                    if(response == 'success'){
                        $('.addUserModal').modal('hide');
                        $scope.addingNewUser = false;
                        $scope.newUserErrors = false;
                        $scope.newUserErrorMessage = "";
                        
                        $scope.newUserKadisID = "";
                        $scope.newUserEntityName = "";


                        getAllUsers();
                    } else if(response == 'not unique'){
                        $scope.newUserErrors = true;
                        $scope.newUserErrorMessage = "This KADIS ID is already in use.";
                    } else {
                        $scope.newUserErrors = true;
                        $scope.newUserErrorMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });
        }


        $scope.toggleRemoveModal = function(user){

            if($scope.removingUser){

                $scope.userToRemove = "";

                $('.removeUserModal').modal('hide');
                $scope.removingUser = false;
            } else {

                $scope.userToRemove = user;

                $('.removeUserModal').modal('show');
                $scope.removingUser = true;

            }
        }
        $('.removeUserModal').on('hidden.bs.modal', function (e) {
            $scope.UserToRemove = "";
            $scope.numberOfMarketsWithUser = "";
            $scope.numberOfUserTypes = "";
            $scope.removingUser = false;
            $scope.$apply();
        })

        $scope.removeUser = function(userToRemove){

            $.ajax({
                type: "POST", 
                url: "users/delete_user",
                data:{
                    "user_id": userToRemove.id
                },
                success: function(response) {
                    if(response == 'success'){
                        
                        $('.removeUserModal').modal('hide');
                        
                        $scope.removingUser = false;
                        $scope.removeUserErrors = false;
                        $scope.removeUserMessage = userToRemove.entity.name + "was successfully removed.";

                        alert($scope.removeUserMessage);
                        
                        getAllUsers();

                    } else {
                        $scope.removeUserErrors = true;
                        $scope.removingUser = true;
                        $scope.removeUserMessage = "There was an error with our server. Please try again.";
                    }
                    $scope.$apply();
                }
            });

        }

        $scope.togglePasswordReset = function(){
            if($scope.resettingPassword){
                $('.resetPasswordModal').modal('hide');
                $scope.resettingPassword = false;
                $scope.password = "";
                $scope.confirmPassword = "";
            } else {
                $('.resetPasswordModal').modal('show');
                $scope.resettingPassword = true;
            }
        }
        $('.resetPasswordModal').on('hidden.bs.modal', function (e) {
            $scope.resettingPassword = false;
            $scope.password = "";
            $scope.confirmPassword = "";
            $scope.$apply();
        })

        $scope.resetPassword = function(){
            if($scope.password != $scope.confirmPassword){
                $scope.errors = true;
                $scope.errorMessage = "Passwords do not match.";

                $scope.$apply();
            }
            else{
                $.ajax({
                    type: "POST", 
                    url: "users/reset_password",
                    data:{
                        "password": $scope.password
                    },
                    success: function(response) {
                        console.log(response);
                        if(response == 'saved'){
                            $scope.errors = false;
                            $scope.errorMessage = "";

                            $scope.success = true;
                            $scope.successMessage = "Password changed!";
                            
                            $scope.togglePasswordReset();
                            getUser();
                        }
                        else{
                            $scope.errors = true;
                            $scope.errorMessage = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }
        }

        $scope.toggleUserPasswordModal = function(user){
            if($scope.resettingUserPassword){
                $('.resetUserPasswordModal').modal('hide');
                
                $scope.resettingUserPassword = false;
                
                $scope.userPassword = "";
                $scope.userConfirmPassword = "";

                $scope.currentUser = "";
                $scope.errors = false;
                $scope.errorMessage = "";
            } else {
                $scope.currentUser = user;
                $('.resetUserPasswordModal').modal('show');
                $scope.resettingUserPassword = true;
            }
        }
        $('.resetUserPasswordModal').on('hidden.bs.modal', function (e) {
            $scope.resettingUserPassword = false;
            $scope.userPassword = "";
            $scope.userConfirmPassword = "";
            $scope.currentUser = "";
            $scope.errors = false;
            $scope.errorMessage = "";
            $scope.$apply();
        })

        $scope.resetUserPassword = function(){
            if($scope.userPassword != $scope.userConfirmPassword){
                $scope.errors = true;
                $scope.errorMessage = "Passwords do not match.";

                // $scope.$apply();
            }
            else{
                $.ajax({
                    type: "POST", 
                    url: "users/reset_user_password",
                    data:{
                        "password": $scope.userPassword,
                        "user_id": $scope.currentUser.id
                    },
                    success: function(response) {
                        console.log(response);
                        if(response == 'saved'){
                            $scope.errors = false;
                            $scope.errorMessage = "";

                            $scope.success = true;
                            $scope.successMessage = "Password changed!";
                            
                            $scope.toggleUserPasswordModal();
                            getUser();
                        }
                        else{
                            $scope.errors = true;
                            $scope.errorMessage = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }
        }

        function getUser(){
            $http.get('users/get_user_info').success(function(data){ 
                $scope.user = data;
                console.log($scope.user);
                $scope.username = $scope.user.username;
                $scope.email = $scope.user.email;

                getAllUsers();
            });
        }

        //get all produce and produce types not in inventory
        function getAllUsers(){
            $http.get('users/get_all_users').success(function(data){ 
                $scope.allUsers = data;
                console.log(data);
            });
        }

    }]);

    /*================
    ACCOUNT CONTROLLER
    ================*/

    mappingApp.controller('AccountController', ['$scope', '$http', function($scope, $http){

        $scope.resettingPassword = false;
        $scope.errors = false;
        $scope.success = false;

        $scope.successMessage = "";
        $scope.errorMessage = "";

        $scope.password = "";
        $scope.confirmPassword = "";

        getUser();

        

        $scope.togglePasswordReset = function(){
            if($scope.resettingPassword){
                $scope.resettingPassword = false;
            } else {
                $scope.resettingPassword = true;
            }
        }

        $scope.resetPassword = function(){
            if($scope.password != $scope.confirmPassword){
                $scope.errors = true;
                $scope.errorMessage = "Passwords do not match.";

                $scope.$apply();
            }
            else{
                $.ajax({
                    type: "POST", 
                    url: "reset_password",
                    data:{
                        "password": $scope.password
                    },
                    success: function(response) {
                        if(response == 'saved'){
                            $scope.errors = false;
                            $scope.errorMessage = "";

                            $scope.success = true;
                            $scope.successMessage = "Password changed!";
                            
                            $scope.togglePasswordReset();
                            getUser();
                        }
                        else{
                            $scope.errors = true;
                            $scope.errorMessage = "There was an error with our server. Please try again.";
                        }
                        $scope.$apply();
                    }
                });
            }
        }

    }]);